## Gate Controller

My property has an old motorized swing gate at the end of the driveway.
This gate can be opened with the keypad on the outside or a garage door controller when within 15 feet.

<img src="https://lh3.googleusercontent.com/pw/AM-JKLWgJDquaYElaP3VBAuR4NmRacMa2EjFbbhZ3Ibj6kYxN2_31nckjwrzYhf4PGvZjg_HWjPEBWUsDvDIrIms9LioXeMqtbHZxjPc3R8ymw0u_yMh6XrdKJKlezy3RnQmvHzM_x_pLNldSHUWRjzseepINg=w923-h720-no?authuser=0" alt="Gate" width="500"/>

This works for coming and going, but lead to the awkward situation of walking 200 ft down the driveway to open the gate by taking the garage door opener out of the car or typing in the gate code backwards through the bars.

This project addresses those problems by adding an additional XBee transmitter and receiver attached to the Guard Station (open/stop/close) ports on the control board.

## Incomplete list of features:

* Open, stop, close gate
* Knows state of gate (open, closed, or in the middle)
* Communicates between gate and home LAN using ZigBee XBee radios, which were needed for the 200ft range where wifi becomes unreliable, and obstacles such as a person, or a wet tree block line of sight. This is why I moved away from nrf24l01 radios. That and their garbage C API.
* Gate receiver can queue commands including waiting. This allows me to "cycle" the gate for a period of time with one button press.
* Gate transmitter is an RPi4 and hosts a local vuejs site on a Pi4. Sends commands, receives status updates from the slave, and sends realtime we socket updates to all clients with the site open
* Transmitter also hosts and API for other clients than just the vueJs site.
* Transmitter is hooked to speakers and can play sounds when the gate opens or closes at certain hours (acts as an alarm after dark)


### Transmitter
<img src="https://lh3.googleusercontent.com/pw/AM-JKLWJ2XSwiReGqnqRUbUt8Y7QYGKDDF3qVmMjGpNIW_aItXHZySERqzna8oNuYZfC6Z4s3Uc9fxikUgrrDX3-kQsJp77XN2cBiyyobpzZoO4F4GIAmihm9sYzU-pyhyPdEcwzPqjn0MgYLqhE2tQgS4JvBg=w445-h939-no?authuser=0" alt="Transmitter Hardware" width="500"/>

### Receiver
<img src="https://lh3.googleusercontent.com/pw/AM-JKLXbheRHqK-4F1fEVhRq_iyGdGzklGS4oeHWv1QxloBNR1BmL5cnlAGCAvT0bIMmAjpRT5wUsnBPZX889BgS49QhL5FTkyqWzvCK91Nd9lY1i9waY2uL4netAml2hfDqJu_Su1982SE81Xn-V7tNQnUVPQ=w705-h939-no?authuser=0" alt="Receiver Hardware" width="500"/>

### Mobile site
<img src="https://lh3.googleusercontent.com/pw/AM-JKLUMaA7YzTUwaDK9HBQHUElLPAzPcNrEVGf-3H4JGQE22Am3KSmCmoljA5_eiIKrb1cMsz-PM__9zm0nYJ8TuTLCcQ9KpXBxq27kceSl_LsyzeyBYcv80Mx7y7ZqNZuMqxET9moKqCBkLJry4PLqQDMigg=w445-h939-no?authuser=0" alt="Mobile Site" width="500"/>

### Full album
(Includes many images of prototypes)
https://photos.app.goo.gl/za9ZuCe2oo9ag2fX6

### Build from source
```
# Base Station API
cd src/gateControllerBaseStation
docker build -t carvellwakeman/gatecontroller .

# Base Station UI
cd src/gateControlUI
docker build -t carvellwakeman/gatecontrollerui .

# Receiver
Arduino IDE (or VS Code w/ Arduino plugin), setup your bootloader and port
Open gateControllerReceiver/gateControllerReceiver.ino
Build
```

### Running Locally
First, build from source.
Then: 

```
cd ./deploy
docker compose up -d
```

### Deployment
This system is intended to be built and deployed with docker.

```
# Build API and UI (interactive)
wget -O - https://gitlab.com/carvellwakeman/gatecontroller/-/raw/master/scripts/build-docker.sh
sudo sh build-docker.sh
```

```
# Deploy API and UI with Docker Compose
wget -O - https://gitlab.com/carvellwakeman/gatecontroller/-/raw/master/scripts/setup.sh
sudo sh setup.sh
```

## Future work
See ![TODO](TODO.md)
