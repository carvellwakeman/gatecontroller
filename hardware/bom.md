* Raspberry pi 4b
  * $35 - out of stock
  * Pi Power supply
    * $10 - https://www.amazon.com/CanaKit-Raspberry-Power-Supply-USB-C/dp/B07TYQRXTK
  * MicroSD card (raspi-os)
    * $4 - https://www.amazon.com/Gigastone-2-Pack-Surveillance-Security-Professional/dp/B0858FBL8V
  * Micro USB cable (short)
    * $2
  * Optional: Speakers w/ 3.5mm jack
* XBee radio  (x2)
  * $63.94 - https://www.amazon.com/gp/product/B013EU6NZS/ref=ppx_yo_dt_b_asin_image_o00_s00?ie=UTF8&psc=1
  * XBee explorer usb (x2)
    * $11.99 - https://www.amazon.com/gp/product/B017KGBP6Y/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1
* Step down converter
  * $1.5 - https://protosupplies.com/product/mp1584-mini-adjustable-dc-dc-step-down-module
* 5v relay breakout (x3)
  * $1.4 - https://www.amazon.com/Relay-Module-Channel-Opto-Isolated-Trigger/dp/B09G6H7JDT
* Barrel plug
  * $0.03
* Proto-board?
  * $0.10
* M&F Connector pair (5 pin)
  * $0.05
* M&F Connector pair (2 pin)
  * $0.05
* Arduino Nano
  * $8 (in bulk)
* 18ga wire (power)
  * varies
* 22ga wire (signal)
  * varies
* RPSMA Antenna (x2)
  * $6.99/2 - https://www.amazon.com/Bingfu-Rosewill-Gigabyte-Wireless-Security/dp/B082SHKT3Q
* RPSMA extension cable (x2)
  * $10.99/2 - https://www.amazon.com/Bingfu-Bulkhead-Antenna-Extension-Wireless/dp/B07SYPDYLW
* Small Plastic Enclosure
  * $1.5 - https://www.amazon.com/gp/product/B07S4L1NHH
* Large Plastic Enclosure
  * $12 - https://www.amazon.com/Plastic-Electronic-Junction-Enclosure-Waterproof/dp/B01N8SLCUM
* Optional: 12v power supply
  * $12.90 - https://www.amazon.com/gp/product/B008EPKLRK/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1
* 