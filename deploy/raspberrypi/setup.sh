#!/bin/bash
# MANUAL: To get this script - git clone https://gitlab.com/carvellwakeman/homeautomation.git
# MANUAL: git config credential.helper store
# ./homeautomation/services/gateController/gateControllerBaseStation/setup/install.sh

# Run in the directory of this script
cd $(dirname $0)

# Raspberry Pi setup
echo Set GPU memory to minimum
sudo raspi-config nonint do_memory_split 16
sudo timedatectl set-timezone US/Pacific #Timezone

echo Create gateMaster user
# sudo useradd -m tempuser -s /bin/bash
# sudo passwd tempuser #enter pwd
# sudo usermod -a -G sudo tempuser
# logout, login as tempuser
# cd /etc #backup files
# sudo tar -cvf authfiles.tar passwd group shadow gshadow sudoers subuid subgid lightdm/lightdm.conf systemd/system/autologin@.service sudoers.d/010_pi-nopasswd systemd/system/getty@tty1.service.d/autologin.conf polkit-1/localauthority.conf.d/60-desktop-policy.conf
# sudo sed -i.$(date +'%y%m%d_%H%M%S') 's/\bpi\b/gateMaster/g' passwd group shadow gshadow sudoers subuid subgid systemd/system/autologin@.service sudoers.d/010_pi-nopasswd systemd/system/getty@tty1.service.d/autologin.conf polkit-1/localauthority.conf.d/60-desktop-policy.conf
# sudo sed -i.$(date +'%y%m%d_%H%M%S') 's/user=pi/user=gateMaster/' lightdm/lightdm.conf
# sudo mv /home/pi /home/gateMaster
# sudo ln -s /home/gateMaster /home/pi
# sudo [ -f /var/spool/cron/crontabs/pi ] && sudo mv -v /var/spool/cron/crontabs/pi /var/spool/cron/crontabs/gateMaster
# sudo passwd gateMaster #enter pwd
# logout, login as gateMaster
# sudo userdel tempuser
# sudo pkill -u tempuser # if userdel fails
# sudo usermod -a -G input gateMaster

# Update Pi
sudo apt update -y
sudo apt full-upgrade -y

# Increase volume
sudo amixer cset numid=1 100%

# Python
# sudo apt-get install python3-pip -y
# sudo pip3 install -r $(realpath ../requirements.txt)
sudo apt install libsdl2-mixer-2.0-0 # Dependency for pygame mixer

# Git
# sudo apt install git -y

# Nginx
sudo apt install nginx
sudo /etc/init.d/nginx start
sudo cp ./nginx.conf /etc/nginx/

# Site (build, deploy)
# curl -sL https://deb.nodesource.com/setup_current.x | bash -
# apt-get install -y nodejs
# cd ../../gateControlUI

# chmod +x ../gateControlUI/scripts/install.sh
# ../gateControlUI/scripts/install.sh

# Redis
# sudo apt install -y redis-server
# sudo service redis-server restart

# Crontab
# MANUAL: sudo crontab -e
# MANUAL: @reboot service redis-server restart
# MANUAL: @reboot (cd /home/gateMaster/gatecontroller/src/gateControllerBaseStation/src/ && python3 ./app.py --release --usb /dev/ttyUSB0 >> /dev/null 2>&1)
# MANUAL: sudo systemctl restart cron

# watch "cat /var/log/cron.log"
