#!/bin/bash

# Run in the directory of this script
cd $(dirname $0)

git clean -xdf
git pull

# Nginx
sudo cp ./nginx.conf /etc/nginx/
nginx -s reload