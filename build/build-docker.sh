#!/bin/bash
# How to run this
# sudo su -c "bash <(wget -O - https://gitlab.com/carvellwakeman/gatecontroller/-/raw/master/build/build-docker.sh)" root

# TODO: Where to run this? Gitlab CI?

# Step 0 - Install git
if [ -x "$(command -v git)" ]; then
    echo "Git already installed"
else
    echo "Install git"
    apt-get install git -y
fi

# Step 1 - install docker (https://docs.docker.com/engine/install/debian)
if [ -x "$(command -v docker)" ]; then
    echo "Docker already installed"
else
    echo "Install docker"
    curl -fsSL https://get.docker.com -o get-docker.sh
    sh get-docker.sh
    systemctl enable docker.service
    systemctl enable containerd.service
    systemctl start docker.service
    systemctl start containerd.service
    rm -f get-docker.sh
fi

# Step 2 - Get input
printf "\n"
read -p "Build gatecontroller y\n? " buildgatecontroller
read -p "Build gatecontrollerui y\n? " buildgatecontrollerui

if [ ! "$buildgatecontroller" == "y" ] && [ ! "$buildgatecontrollerui" == "y" ]; then
    printf "No targets selected, exiting...\n"
    exit 1
fi

# Step 3 - Get repo
read -p "Which branch? " repobranch

# Step 4 - Get release version
read -e -p "What is the release version? (enter for latest): " dockertag

printf "---------BUILDING---------"

# Step 7 - Login
docker login

printf "Removing old repo if present\n"
sudo rm -rf ./gatecontroller

printf "Cloning repository\n"
git clone https://gitlab.com/carvellwakeman/gatecontroller.git
cd ./gatecontroller
git checkout "$repobranch"
cd ../

# Step 4 - Cleanup old docker stuff
# docker rm -vf $(docker ps -aq)
# docker rmi -f $(docker images -aq)

# Step 5a - Build the builder
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx create --name multiarch --driver docker-container --use
docker buildx inspect --bootstrap

# Step 5 - Build api
if [ "$buildgatecontroller" == "y" ]; then
    printf "Building GateController Api\n"
    cd ./gatecontroller/src/gateControllerBaseStation
    docker buildx build --platform linux/arm/v7,linux/amd64 -t carvellwakeman/gatecontroller:$dockertag --cache-from carvellwakeman/gatecontroller:latest --pull --push .
    # docker tag carvellwakeman/gatecontroller carvellwakeman/gatecontroller
    # docker push carvellwakeman/gatecontroller
    cd ../../../
fi

# Step 6 - Build ui
if [ "$buildgatecontrollerui" == "y" ]; then
    printf "Building GateControllerUI\n"
    cd ./gatecontroller/src/gateControlUI
    docker buildx build --platform linux/arm/v7,linux/amd64 -t carvellwakeman/gatecontrollerui:$dockertag --cache-from carvellwakeman/gatecontroller:latest --pull --push .
    # docker tag carvellwakeman/gatecontrollerui carvellwakeman/gatecontrollerui
    # docker push carvellwakeman/gatecontrollerui
    cd ../../../
fi


printf "\n"
printf "Done!\n"

# Cleanup
sudo rm -rf ./gatecontroller