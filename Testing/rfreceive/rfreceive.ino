/*
 * nRF24L01 Receiver Test Software
 * 
 * Exercises the nRF24L01 Module.  This code runs on the receiver 'slave' device.
 * Use the nRF24L01_Transmitter_Test software for the transmitting 'master' device
 * 
 * This uses the RF24.h library which can be installed from the Arduino IDE
 * Pins used for the SPI interface are determined by the Arduino being used. 
 * The other two pins are arbitrary and can be changed if needed.  Redfine them in the RF24  
 * statement.  Default shown here is to use pins 7 &
 */
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
RF24 radio(7,8); // CE, CSN      // Define instance of RF24 object called 'radio' and define pins used
const byte address[6] = "00001";  // Define address/pipe to use. This can be any 5 alphnumeric letters/numbers
unsigned long last = -1;
unsigned long errs = 0;
//===============================================================================
//  Initialization
//===============================================================================
void setup() {
  Serial.begin(9600);                // Start serial port to display messages on Serial Monitor Window
  radio.begin();                     // Start instance of the radio object
  radio.openReadingPipe(0, address); // Setup pipe to write data to the address that was defined
  radio.setPALevel(RF24_PA_MIN);     // Set the Power Amplified level to MAX in this case
  radio.startListening();            // We are going to be the receiver, so we need to start listening

  //pinMode(10, OUTPUT);
}
//===============================================================================
//  Main
//===============================================================================
void loop() {
  if (radio.available()) {  
    unsigned long command = 0;               // Clear buffer
    radio.read(&command, sizeof(command));  // Read incoming message into buffer
    Serial.print("Received: "); Serial.print(command); Serial.print(" Errors: "); Serial.println(errs);

    if (command != last+1) {
      errs ++;
    }

    last = command;
    // digitalWrite(10, HIGH);
    // delay(250);
    // digitalWrite(10, LOW);
    // delay(250);
  }
}