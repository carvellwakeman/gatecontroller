
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

// nRF24L01
#define PIN_CE 7
#define PIN_CSN 8
RF24 radio(PIN_CE, PIN_CSN); // CE, CSN
const byte radioAddress[6] = "00001";  // Define address/pipe to use.


#define TRANSMITTER false

void setup() {
  // put your setup code here, to run once:
  // Serial
  Serial.begin(9600);

  if (TRANSMITTER) {
    // nRF24L01
    radio.begin();                  // Start instance of the radio object
    radio.openWritingPipe(radioAddress); // Setup pipe to write data to the address that was defined
    radio.setPALevel(RF24_PA_MAX);  // Set the Power Amplified level to MAX in this case
    radio.stopListening();          // We are going to be the transmitter, so we will stop listening
  } else {
    radio.begin();                     // Start instance of the radio object
    radio.openReadingPipe(0, radioAddress); // Setup pipe to write data to the address that was defined
    radio.setPALevel(RF24_PA_MAX);     // Set the Power Amplified level to MAX in this case
    radio.startListening();            // We are going to be the receiver, so we need to start listening

    pinMode(10, OUTPUT);
  }
}

void loop() {
  if (!TRANSMITTER) {
    // Receive radio data
    char cmd = receiveCommand();
    if (cmd > 0) {
      digitalWrite(10, HIGH);
      delay(1000);
      digitalWrite(10, LOW);
      delay(1000);
    }
  } else {
    sendCommand('a');
    delay(1000);
  }
}

//==============================================================================
// nRF24L01 methods
//==============================================================================
void sendCommand(char command) {
    Serial.print("Sending: "); Serial.println(command);
    radio.write(&command, sizeof(command));
}

char receiveCommand() {
    if (radio.available()) {  
        char instruction;
        radio.read(&instruction, sizeof(instruction));  // Read incoming message into buffer
        Serial.println(instruction);             // Print the message to the Serial Monitor window
        return instruction;
        //addInstruction(instruction);
    }
    return 0;
}
