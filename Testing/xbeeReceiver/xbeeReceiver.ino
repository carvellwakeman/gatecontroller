#include <XBee.h>
#include <SoftwareSerial.h>

/*
This example is for Series 2 (ZigBee) XBee Radios only
Receives I/O samples from a remote radio.
The remote radio must have IR > 0 and at least one digital or analog input enabled.
The XBee coordinator should be connected to the Arduino.
 
This example uses the SoftSerial library to view the XBee communication.  I am using a 
Modern Device USB BUB board (http://moderndevice.com/connect) and viewing the output
with the Arduino Serial Monitor.
*/

// Define NewSoftSerial TX/RX pins
#define PIN_DOUT 22 //GP22
#define PIN_DIN 21 //GP21
SoftwareSerial nss(PIN_DOUT, PIN_DIN);

XBee xbee = XBee();

XBeeAddress64 test = XBeeAddress64();

void setup()
{
    Serial.begin(9600);
    // start soft serial
    nss.begin(9600);
    xbee.setSerial(nss);

    Serial.println("Begin");
}

void loop()
{
    //attempt to read a packet
    xbee.readPacket();

    if (xbee.getResponse().isAvailable())
    {
        Serial.println("Response isAvailable()");
        if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE)  // RX_16_RESPONSE
        {
            // When frame char is 0,0,2, the next char is the ascii data
            // Frame: <junk/meta> 0 0 2 <the data>
            if (xbee.getResponse().getFrameData()[8] == 0 &&
                xbee.getResponse().getFrameData()[9] == 0 &&
                xbee.getResponse().getFrameData()[10] == 2) {

                char buff[xbee.getResponse().getFrameDataLength() - 10];
                for (int i = 11; i < xbee.getResponse().getFrameDataLength(); i++) {
                    buff[i-11] = xbee.getResponse().getFrameData()[i];
                    // Serial.print("byte [");
                    // Serial.print(i, DEC);
                    // Serial.print("] is ");
                    // Serial.print(xbee.getResponse().getFrameData()[i], HEX);
                    // Serial.print(" (");
                    // Serial.write(xbee.getResponse().getFrameData()[i]);
                    // Serial.print(")");
                    // Serial.println();
                }
                Serial.println(buff);
                // Serial.print("Found data");
            }

            // method for printing the entire frame data
            // for (int i = 0; i < xbee.getResponse().getFrameDataLength(); i++)
            // {
            //     buff[i] = xbee.getResponse().getFrameData()[i];

            //     Serial.print("byte [");
            //     Serial.print(i, DEC);
            //     Serial.print("] is ");
            //     // When frame char is 0,0,2, the next char is the ascii data
            //     Serial.print(xbee.getResponse().getFrameData()[i], HEX);
            //     Serial.print(" (");
            //     Serial.write(xbee.getResponse().getFrameData()[i]);
            //     Serial.print(")");
            //     Serial.println();
            // }
        }
        else
        {
            Serial.print("Expected I/O Sample, but got ");
            Serial.print(xbee.getResponse().getApiId(), HEX);
        }
    }
    else if (xbee.getResponse().isError())
    {
        Serial.print("Error reading packet.  Error code: ");
        Serial.println(xbee.getResponse().getErrorCode());
    }
}
