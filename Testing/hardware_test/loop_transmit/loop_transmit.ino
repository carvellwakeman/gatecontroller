
/******************************************************************************
* Loop Transmitter for testing
*******************************************************************************/

#include "Arduino.h"
#include <SPI.h>
#include <XBee.h>
#include <SoftwareSerial.h>

// Debug
#define DEBUG

// RadioSoftwareSerial
#define PIN_DOUT 22 //GP22
#define PIN_DIN 21 //GP21
SoftwareSerial nss(PIN_DOUT, PIN_DIN);
XBee xbee = XBee();

void setup()
{
    // Logging
    Serial.begin(115200);

    // Radio
    nss.begin(9600);
    xbee.setSerial(nss);
    
    Serial.println();
    Serial.println("*** Finished Boot (Loop Transmitter)");
}

void loop()
{
    sendState('a');
    delay(1000);
}


//==============================================================================
// Xbee methods
//==============================================================================
void sendState(char state)
{
    // Transmit to serial
    // XBeeAddress64 addr64 = XBeeAddress64(0x0013A200, 0x41C429AF);
    // ZBTxRequest zbTx = ZBTxRequest(addr64, state, sizeof(state));

    // Broadcast
    // ZBTxRequest zbTx = ZBTxRequest();
    // zbTx.setPayload(state);
    // zbTx.setPayloadLength(sizeof(state));

    // Hacky thing because sending a single char always sends 00
    uint8_t payload[1];
    payload[0] = state;

    XBeeAddress64 addr64 = XBeeAddress64(0x00000000, 0x0000FFFF); // Means broadcast
    ZBTxRequest zbTx = ZBTxRequest(addr64, payload, sizeof(payload));

    xbee.send(zbTx);
    Serial.print("SENT ");
    Serial.println(state);
}
