
/******************************************************************************
 * Loop Receiver for testing
 *******************************************************************************/

#include "Arduino.h"
#include <SPI.h>
#include <XBee.h>
#include <SoftwareSerial.h>

// Debug
#define DEBUG

// // RadioSoftwareSerial
#define PIN_DOUT 22 // GP22
#define PIN_DIN 21	// GP21
SoftwareSerial nss(PIN_DOUT, PIN_DIN);

XBee xbee = XBee();
uint8_t payload[] = {0, 0}; // iniate array byte for receive because 2 char  H,I
ZBRxResponse zbRx = ZBRxResponse();
ModemStatusResponse msr = ModemStatusResponse();
// SH + SL Address of receiving XBee 
XBeeAddress64 addr64 = XBeeAddress64(0x0013A200, 0x41F4C23D); // just change to other address you want forward
ZBTxRequest zbTx = ZBTxRequest(addr64, payload, sizeof(payload));
ZBTxStatusResponse txStatus = ZBTxStatusResponse();

void setup()
{
	// Logging
	Serial.begin(115200);

	// Radio
	nss.begin(9600);
	xbee.setSerial(nss);

	Serial.println();
	Serial.println("*** Finished Boot (Loop Receiver)");
}

void loop()
{
	// 1. This will read any data that is available:
	xbee.readPacket();

	// 2. Now, to check if a packet was received:
	if (xbee.getResponse().isAvailable())
	{
		// got packet from xbee
		if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE)
		{
			// trx packet from xbee to variable
			Serial.println();
			Serial.print("Got an rx packet from: ");
			XBeeAddress64 senderLongAddress = zbRx.getRemoteAddress64();
			print32Bits(senderLongAddress.getMsb());
			Serial.print(" ");
			print32Bits(senderLongAddress.getLsb());
			Serial.println();

			xbee.getResponse().getZBRxResponse(zbRx);
			if (zbRx.getOption() == ZB_PACKET_ACKNOWLEDGED)
			{
				// the sender got an ACK
				Serial.println("packet acknowledged");
			}
			else
			{
				// we got it (obviously) but sender didn't get an ACK
				Serial.print("packet not acknowledged - ");
				Serial.println(zbRx.getOption());
			}
			Serial.print("Packet length is ");
			Serial.print(zbRx.getPacketLength(), DEC);
			Serial.print(", data payload length is ");
			Serial.print(zbRx.getDataLength(), DEC);
			Serial.print(" checksum is ");
			Serial.println(zbRx.getChecksum(), HEX);
			Serial.println("Received Data: ");

			for (int i = 0; i < zbRx.getDataLength(); i++)
			{
				print8Bits(zbRx.getData()[i]); //
				payload[i] = zbRx.getData()[i];
				Serial.print(' ');
			}

			Serial.println();

			// getFrame for format api start delimiter 0x7E bla bla
			// dataOffset for payload packet
			// getdatalength for data payload length
			zbTx.setPayload(zbRx.getFrameData() + zbRx.getDataOffset(), zbRx.getDataLength());
			xbee.send(zbTx);

			Serial.println("Forward packet");
		}
		else
		{
			Serial.println("Not packet received, other status");
		}
	}
	else if (xbee.getResponse().isError())
	{
		Serial.print("error code:");
		Serial.print(xbee.getResponse().getErrorCode());

		if (xbee.getResponse().getErrorCode() == CHECKSUM_FAILURE)
		{
			Serial.println(" Checksum error");
		}
		else if (xbee.getResponse().getErrorCode() == UNEXPECTED_START_BYTE)
		{
			Serial.println(" Unexpected Start Byte");
		}
		else
		{
			Serial.println(" Some other error code");
		}
	}
	//  else {
	//      Serial.println("packet loss");
	//  }
	 delay(500);
} // END OF LOOP

void print32Bits(uint32_t dw)
{
	print16Bits(dw >> 16);
	print16Bits(dw & 0xFFFF);
}

void print16Bits(uint16_t w)
{
	print8Bits(w >> 8);
	print8Bits(w & 0x00FF);
}

void print8Bits(byte c)
{
	uint8_t nibble = (c >> 4);
	if (nibble <= 9)
		Serial.write(nibble + 0x30);
	else
		Serial.write(nibble + 0x37);

	nibble = (uint8_t)(c & 0x0F);
	if (nibble <= 9)
		Serial.write(nibble + 0x30);
	else
		Serial.write(nibble + 0x37);
}