# My S/N is 13A20041C429AF
from digi.xbee.devices import XBeeDevice, RemoteXBeeDevice, XBee64BitAddress

def my_data_received_callback(xbee_message):
    address = xbee_message.remote_device.get_64bit_addr()
    data = xbee_message.data.decode("utf8")
    print("\n%s says: %s" % (address, data))

try:
    local_xbee = XBeeDevice("COM5", 9600)
    local_xbee.open()

    # Send data to remote device
    # remote_xbee = RemoteXBeeDevice(local_xbee, XBee64BitAddress.from_hex_string("13A20041C42A01"))
    # local_xbee.send_data(remote_xbee, "Hello receiver!")
    # local_xbee.send_data_broadcast("Hello All!")

    # Read data sent by the remote device.
    # xbee_message = local_xbee.read_data(remote_xbee)
    # xbee_message = local_xbee.read_data()
    # print(xbee_message)
    
    # Add the callback.
    local_xbee.add_data_received_callback(my_data_received_callback)

    print("Send:", end='')
    while True:
        msg = input()
        # remote_xbee = RemoteXBeeDevice(local_xbee, XBee64BitAddress.from_hex_string("0013A20041C42A01"))
        # local_xbee.send_data(remote_xbee, msg)

        local_xbee.send_data_broadcast(msg)
        print("Send:", end='')

    local_xbee.close()

finally:
    if local_xbee is not None and local_xbee.is_open():
        local_xbee.del_data_received_callback(my_data_received_callback)
        local_xbee.close()

