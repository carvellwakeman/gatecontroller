from digi.xbee.devices import XBeeDevice, RemoteXBeeDevice, XBee64BitAddress, XBee16BitAddress
from digi.xbee.models.message import XBeeMessage
from digi.xbee.models.atcomm import ATStringCommand
from digi.xbee.exception import *
import time

# radioPort = "COM4"
radioPort = "/dev/ttyUSB0"

local_xbee = XBeeDevice(radioPort, 9600)


local_xbee.open()


def getDb(res):
    atCmd = res
    atResult = atCmd
    utfResult = atResult.decode('utf8')

    rssi = ord(utfResult) * -1 if utfResult else 0

    print(str(atResult) + " " + str(utfResult) + " " + str(rssi))


while (True):
    try:
        packet = "4,1"
        remote_xbee = RemoteXBeeDevice(local_xbee, XBee64BitAddress.from_hex_string('0013A20041F4C1EF'))


        response = local_xbee.send_data(remote_xbee, packet)


        description = response.transmit_status.description
        retries = response.transmit_retry_count
        success = response.transmit_status.code == 0

        print(str(description) + " " + str(retries) + " " + str(success))

        command = "DB"
        getDb(local_xbee.get_parameter(command))
        time.sleep(2)
        getDb(remote_xbee.get_parameter(command))

       
    except Exception as ex:
        print("failed: " + str(ex))
        pass

    time.sleep(2)

    
local_xbee.close()