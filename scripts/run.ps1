$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath
Write-host "My directory is $dir"
Push-Location $dir

$dir = $(Resolve-Path $(Join-Path $dir '..'))


# build api
cd $(Resolve-Path $(Join-Path $dir './src/gateControllerBaseStation'))
docker build -t carvellwakeman/gatecontroller .

# build ui
cd $(Resolve-Path $(Join-Path $dir './src/gateControlUI'))
docker build -t carvellwakeman/gatecontrollerui .

# run
cd $(Resolve-Path $(Join-Path $dir './deploy'))
docker compose up

Pop-Location