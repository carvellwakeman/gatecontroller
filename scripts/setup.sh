#!/bin/bash
# How to run this
# sudo su -c "bash <(wget -O - https://gitlab.com/carvellwakeman/gatecontroller/-/raw/master/scripts/setup.sh)" root

sleep 3
# Step 0 - Get repo
read -e -p "Which branch? (enter for master): " -i "master" repobranch

# Step 0 - Get Tag
read -e -p "Which API tag? (enter for latest): " -i "latest" dockertag_api
read -e -p "Which UI tag? (enter for latest): " -i "latest" dockertag_ui

# Step 0 - OS configure
if [ id -u "gatemaster" >/dev/null 2>&1 ]; then
    echo "Gatemaster user exists"
else
    adduser --home /home/gatemaster gatemaster
    groupadd docker
    usermod -aG docker gatemaster
    chsh -s /bin/bash gatemaster
    apt-get update -y
    apt-get upgrade -y
    apt-get install curl usbutils sudo -y
fi

# Step 1 - install docker (https://docs.docker.com/engine/install/debian)
if [ -x "$(command -v docker)" ]; then
    echo "Docker already installed"
else
    echo "Install docker"
    curl -fsSL https://get.docker.com -o get-docker.sh
    sh get-docker.sh
    systemctl enable docker.service
    systemctl enable containerd.service
    systemctl start docker.service
    systemctl start containerd.service
    rm -f get-docker.sh
fi

# Step 2 - cleanup existing containers
printf "\n"
echo "Stopping existing apps and pulling new images"
docker stop gatecontroller --force
docker stop gatecontrollerui --force
docker rm gatecontroller --force
docker rm gatecontrollerui --force
# docker rmi carvellwakeman/gatecontroller --force
# docker rmi carvellwakeman/gatecontrollerui --force
docker pull carvellwakeman/gatecontroller:$dockertag_api
docker pull carvellwakeman/gatecontrollerui:$dockertag_ui

# Step 3 - create directory and pull docker-compose file and service
echo "Gathering docker compose file and systemd service file"
rm -rf /home/gatemaster/data_backup
mkdir /home/gatemaster/data_backup
mv /home/gatemaster/gatecontroller/data /home/gatemaster/data_backup #backup data dir
rm -rf /home/gatemaster/gatecontroller # remove old version
mkdir /home/gatemaster/gatecontroller
mkdir /home/gatemaster/gatecontroller/data
mv /home/gatemaster/data_backup/* /home/gatemaster/gatecontroller/data # restore data backup

# Base docker compose
wget -P /home/gatemaster/gatecontroller \
    https://gitlab.com/carvellwakeman/gatecontroller/-/raw/$repobranch/deploy/docker-compose.yml

# Environment specific docker compose
declare env_found=0
while [ "$env_found"=0 ]; do
    printf "\n"
    printf "Please Specify runtime environment.\n"
    read -p "(ex: for docker-compose.staged.yml, type \"staged\"): " environment

    if [ "$environment" ];  then
        declare wget_result="$(wget -P /home/gatemaster/gatecontroller https://gitlab.com/carvellwakeman/gatecontroller/-/raw/$repobranch/deploy/docker-compose.$environment.yml 2>&1)"

        if echo "$wget_result" | grep -q "200 OK"; then
            printf "File downloaded\n"
            env_found=1
        else
            printf "File not found docker-compose.$environment.yml, try again\n"
            continue
        fi
        
        mv /home/gatemaster/gatecontroller/docker-compose.$environment.yml /home/gatemaster/gatecontroller/docker-compose.override.yml 
        break
    fi

    printf "No environment selected! Try again.\n"
done

# Set environment variables for app versions
# cd /home/gatemaster/gatecontroller
# touch .env
# export TAG_API="$dockertag_api"
# export TAG_UI="$dockertag_ui"
# cd /home/gatemaster

# Get gate controller systemd service
wget -P /home/gatemaster/gatecontroller \
    https://gitlab.com/carvellwakeman/gatecontroller/-/raw/$repobranch/deploy/gate-controller.service

# Step 4 - Setup docker container to run on startup
echo "Configuring systemd service to run on startup"
systemctl stop gate-controller
cp /home/gatemaster/gatecontroller/gate-controller.service /etc/systemd/system/gate-controller.service
systemctl daemon-reload
systemctl enable gate-controller
systemctl start gate-controller

# Step 5 - Wait until service is running
echo "Done."