## Hardware-Transmitter:
- [x] Place into dedicated container
- [ ] Dedicated 3D printed enclosure
- [x] BOM
- [ ] Assembly instructions


## Hardware-Receiver:
- [x] Replace with v2 with enclosure
- [x] Mount antenna higher up on gate
- [x] Re-wire open/close Button inputs with new connector instead of JST pins
- [x] Water insulate around openings more
- [x] Debug plugs for "state" port to mock the gate status (open/moving/closed)
- [ ] Dedicated 3D printed enclosure which also holds relay board
- [x] BOM
- [ ] Assembly instructions
- [ ] Custom PCB


## Software-API:
- [ ] Support for DB stored configuration
- [ ] Better management of configuration from environment variables
- [ ] Move environment.soundNotifyWindowStart and environment.soundNotifyWindowEnd into configuration
- [ ] Allow open/close/cycle endpoints to happen silently (without alarm)
- [ ] Endpoint to clear receiver command queue
- [x] Configuration for receiver ID
- [ ] Store and display build version
- [ ] Tx and Rx Ids no longer necessary, bond with ID of first radio contacted


## Software-UI
- [x] Build VueJS site
- [x] Mobile support on site
- [x] Users, credentials, roles, login  
- [ ] Add banner for non-prod environments  
- [ ] Support for cycling with custom time
- [ ] App notifications on state change
- [x] Fix login page scaling on mobile
- [ ] Support for clearing receiver queue
- [ ] Guest user can see current state
- [x] Site indicates when it's connected to websockets or not
- [ ] Temporary toggle to disable alarm for the next action
- [ ] Store and display build version


## Software-Receiver
- [x] Support clearing current command queue
- [x] Easier debug mode to determine how receiver is behaving and view its internal queue


## Software App
- [x] Mobile app (just a webview wrapper)
- [x] Add option to configure and save URL in the app
- [x] Disable cache or clear it when clicking refresh button


## General
- [x] Better setup and separation for dev environment vs production
- [ ] Backend python tests


## Bugs
- [ ] load request history when receiving a new event (websockets?)
- [ ] Shutdown and restart don't work from container


## DevOps
- [ ] Pass usb device to container to allow a staged environment
- [ ] Proper build pipeline with jenkins/teamcity/octo/etc
- [ ] Build pipeline runs unit tests


## Release 2.0 - Docker transition
- [x] Build api dockerfile
- [x] Setup build and deploy scripts
- [x] Create staged environment and build agent
- [x] Audio engine cross-platform
- [x] read/write gate.db from file system
- [x] Env vars from docker env file instead of python args
- [x] Dockerize UI
- [x] Configure UI and API for multiple production environments
- [x] Figure out a solution to push updates
- [x] Test deploy to a new pi
- [x] Test deploy to my pi (weekend)
- [X] Build receiver
- [x] Test radios (manually) and via rx/tx
- [x] Update firmware for receiver to handle different signal combos for open/moving/closed
- [x] Setup script should take a branch name to pull files from
- [x] Audio files from relative directory not correct

## hotfix 2.0.2
- [x] Pass through more in depth errors from radio
- [ ] Get current status becomes a blocking call that returns actual status
- [x] History pane shows most recent request as ongoing, once request is made data is refreshed and an optional error can be shown.
- [x] Http transport errors show up as toasts
- [x] UptimeWorker polls the receiver every 10 seconds and records the result with a timestamp.
- [x] Uptime data can be displayed
- [ ] Uptime worker can be disabled from settings