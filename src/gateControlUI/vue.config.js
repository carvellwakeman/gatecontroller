const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
  "indexPath": "index.html",
  "transpileDependencies": [
    "vuetify"
  ],
  "chainWebpack": config => {
    if (process.env.NODE_ENV === 'production') {
        // config.plugins.delete('html')
        config.plugins.delete('preload')
        config.plugins.delete('prefetch')
        config
        // .plugin('limitChunkCountPlugin')
        // .use(new webpack.optimize.LimitChunkCountPlugin({
        //   maxChunks: 1
        // }))
        // .end()
        .plugin('extract-css')
          .tap((args) => {
            // Flatten .css output: ./css/*.css -> ./*.css
            args[0].filename = '[name].css';
            args[0].chunkFilename = '[name].css';
            return args;
          })
        .end()
        .module
          .rule('images')
            .use('url-loader')
              .tap((args) => {
                // Flatten bitmap img output: ./img/* -> ./*
                args.fallback.options.name = '[name].[contenthash:8].[ext]';
                return args;
              })
              .end()
            .end()
          .rule('svg')
            .use('file-loader')
              .tap((args) => {
                // Flatten svg output: ./img/* -> ./*
                args.name = '[name].[ext]';
                return args;
              })
              .end()
            .end()
          .rule('media')
            .use('url-loader')
              .tap((args) => {
                // Flatten AV output: ./media/* -> ./*
                args.fallback.options.name = '[name].[ext]';
                return args;
              })
              .end()
            .end()
          .rule('fonts')
            .use('url-loader')
              .tap((args) => {
                // Flatten fonts output: ./fonts/* -> ./*
                args.fallback.options.name = '[name].[ext]';
                return args;
              })
              .end()
            .end()
          .end();
    }
  },
  configureWebpack: {
      // Flatten .js output: ./js/*.js -> ./*.js
      output: {
        filename: '[name].js',
        chunkFilename: '[name].js'
      },

      plugins: [
        new CopyWebpackPlugin(
          [
           {
             from: path.join(__dirname, 'public'),
             to: path.join(__dirname, 'dist'),
             toType: "dir",
            //  ignore: [ "index.html", ".DS_Store" ]
           }
          ]
        )
      ],

      // Fix relative url() in CSS
      // Otherwise Vue CLI will generate '../' as the publicPath for the 
      // 'mini-css-extract-plugin' which is wrong as we flatten everything
      // See: https://github.com/vuejs/vue-cli/blob/b156ff17e520d0653d5dbcc8e18d19ecd2ab4798/packages/%40vue/cli-service/lib/config/css.js#L64
      // module.rules
      //   .flatMap((rule) => rule.oneOf)
      //   .filter((oneOf) => !!oneOf)
      //   .flatMap((oneOf) => oneOf.use)
      //   .filter((use) => use.loader.includes('mini-css-extract-plugin'))
      //   .forEach((use) => use.options.publicPath = './');    
  }
}