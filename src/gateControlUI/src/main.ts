import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import store from './store/store'
import vuetify from './plugins/vuetify';
import VueSocketIOExt from 'vue-socket.io-extended';
import { io } from 'socket.io-client';
import interceptorsSetup from './interceptors'
// @ts-ignore
import { baseURL } from '@/app.config';
import VueApexCharts from 'vue-apexcharts'

Vue.config.productionTip = false

Vue.filter('uppercase', function (value: any) {
	return value.toUpperCase()
})

// Websockets
console.log("My BaseUrl is:");
console.log(baseURL);
const socket = io(baseURL);

Vue.use(VueSocketIOExt, socket);
Vue.use(VueApexCharts);
Vue.component('apexchart', VueApexCharts)

// Axios setup
interceptorsSetup()

// Vue
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
