export const uiVersion = process.env.VUE_APP_VERSION
export const baseURL = process.env.VUE_APP_API_BASE_URL

export default {
    uiVersion,
    baseURL,
}