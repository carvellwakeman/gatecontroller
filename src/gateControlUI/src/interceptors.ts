import axios from 'axios';
import store from '@/store/store'

export default function setup() {
    // Send token interceptor
    axios.interceptors.request.use(function (config) {
        let token = store.state.token;
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    }, function (err) {
        return Promise.reject(err);
    });

    // Add a 401 response interceptor
    axios.interceptors.response.use(function (response) {
        return response;
    }, function (error) {
        if (error.response && error.response.status === 401) {
            store.dispatch("logout");
        }

        return Promise.reject(error);
    });

    // Global validate status
    axios.defaults.validateStatus = (status) => {
        return status < 300;
    };
}