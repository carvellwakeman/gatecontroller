import Vue from 'vue'
import Vuex from 'vuex'
import GateApi from "@/api/api";
import StateHistoryItem from "@/api/StateHistoryItem";
import RequestHistoryItem from "@/api/RequestHistoryItem";
import EventHistoryItem from "@/models/EventHistoryItem";
import UserModel from '@/api/UserModel';
import Cookies from 'js-cookie'
import ApiConfig from '@/api/ApiConfig';
import PollingHistoryItem from '@/api/PollingHistoryItem';
import SystemConfigurationModel from '@/api/SystemConfigurationModel';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    apiConfig: ApiConfig,
    latestState: "UNKNOWN",
    stateHistory: Array<StateHistoryItem>(),
    requestHistory: Array<RequestHistoryItem>(),
    eventHistory: Array<EventHistoryItem>(),
    pollingHistory: Array<PollingHistoryItem>(),
    users: Array<UserModel>(),
    systemConfigurations: Array<SystemConfigurationModel>(),
    token: null,
    loggedInUser: null,
    errors: []
  },
  getters: {
    isTokenExpired(state: any): boolean {
      let parsedTokenHeader = JSON.parse(atob(state.token.split('.')[0]));

      let tokenExpiration = new Date(parsedTokenHeader.exp * 1000);
      if (tokenExpiration > new Date()) {
        return true;
      }
      return false;
    },
    userRole(state: any): string | null {
      if (state.loggedInUser != null) return null;
      return state.loggedInUser.role;
    },
    isAdminRole(state: any): boolean {
      if (state.loggedInUser == null) return false;
      return state.loggedInUser.role == 'admin';
    },
    isUserRole(state: any): boolean {
      if (state.loggedInUser == null) return false;
      return state.loggedInUser.role == 'user';
    },
    isGuestRole(state: any): boolean {
      if (state.loggedInUser == null) return false;
      return state.loggedInUser.role == 'guest';
    },
    getErrorCount(state: any): number {
      return state.errors.length;
    },
    getPollingInterval(state: any): SystemConfigurationModel | null {
      return state.systemConfigurations?.filter( (sc: SystemConfigurationModel) => sc.systemConfigurationType == 'polling_interval')[0]
    },
    getPollingEnabled(state: any): SystemConfigurationModel | null {
      return state.systemConfigurations?.filter( (sc: SystemConfigurationModel) => sc.systemConfigurationType == 'polling_enabled')[0]
    }
  },
  mutations: {
    setApiConfig(state: any, apiConfig: ApiConfig) {
      state.apiConfig = apiConfig;
    },
    setLatestState(state: any, latestState: String) {
      state.latestState = latestState;
    },
    setStateHistory(state: any, history: StateHistoryItem[]) {
      state.stateHistory = history;
    },
    setRequestHistory(state: any, history: RequestHistoryItem[]) {
      state.requestHistory = history;
    },
    setPollingHistory(state: any, history: PollingHistoryItem[]) {
      state.pollingHistory = history;
    },
    addRequestHistory(state: any, historyItem: RequestHistoryItem) {
      state.requestHistory.push(historyItem);
    },
    addError(state: any, error: Error) {
      state.errors.push(error);
    },
    setUsers(state: any, users: UserModel[]) {
      state.users = users;
    },
    setSystemConfiguration(state: any, configuration: SystemConfigurationModel[]) {
      state.systemConfigurations = configuration;
    },
    setToken(state: any, token: string) {
      state.token = token;

      if (token == null || token == undefined) {
        Cookies.remove("token");
      } else {
        Cookies.set("token", token);
      }

      if (state.token == null || state.token == undefined) {
        state.loggedInUser = null;
        return;
      }
      let parsedTokenPayload = JSON.parse(atob(state.token.split('.')[1]));
      state.loggedInUser = parsedTokenPayload as UserModel;
    },
    setLoggedInUser(state: any, loggedInUser: UserModel) {
      state.loggedInUser = loggedInUser;
    }
  },
  actions: {
    async loadApiConfig({ getters, commit }: any, api: GateApi) {
      await api.appConfig()
        .then((response) => {
          commit('setApiConfig', response);
        })
        .catch((err) => err);
    },
    async loadStateHistory({ getters, commit }: any, api: GateApi) {
      if (getters.isGuestRole) {
        return;
      }

      await api.getStateHistory()
        .then((history) => {
          commit('setStateHistory', history);
        })
        .catch((err) => err);
    },
    async loadRequestHistory({ getters, commit }: any, api: GateApi) {
      if (getters.isGuestRole) {
        return;
      }

      await api.getRequestHistory()
        .then((history) => {
          commit('setRequestHistory', history);
        })
        .catch((err) => err);
    },
    async loadCurrentState({ commit, dispatch }: any, api: GateApi) {
      await api.getCurrentState()
        .then((latestState) => {
          commit('setLatestState', latestState?.state);
        })
        .catch((err) => err);
    },
    async loadUsers({ commit }: any, api: GateApi) {
      await api.getUsers()
        .then((users) => {
          commit('setUsers', users);
        })
        .catch((err) => err);
    },
    async loadSystemConfiguration({ commit }: any, api: GateApi) {
      await api.getSystemConfigurations()
        .then((configuration) => {
          commit('setSystemConfiguration', configuration);
        })
        .catch((err) => err);
    },
    async loadPollingHistory({ getters, commit }: any, api: GateApi) {
      await api.getPollingHistory()
        .then((history) => {
          commit('setPollingHistory', history);
        })
        .catch((err) => err);
    },
    setCurrentState({ commit }: any, newState: String) {
      let history = this.state.stateHistory;
      history.push(new StateHistoryItem(-1, 'Fake', newState, new Date().toString()))
      commit('setStateHistory', history)
    },
    login({ commit, getters }: any, { api, username, password }: any) {
      return new Promise((resolve, reject) => {
        return api.login(username, password)
          .then((token: string) => {
            commit('setToken', token);
            resolve(token);
          })
          .catch((err: any) => {
            reject(err);
          });
      });
    },
    logout({ commit }: any) {
      commit('setToken', null);
    },
    async openGate({ dispatch, commit }: any, { api, silent }: any) {
      commit('addRequestHistory', new RequestHistoryItem(-1, 'Open', true, '', new Date().toISOString()));
      let result = await api.openGate(silent);
      dispatch('loadRequestHistory', api);
      return result?.success;
    },
    async stopGate({ dispatch, commit }: any, api: GateApi) {
      commit('addRequestHistory', new RequestHistoryItem(-1, 'Stop', true, '', new Date().toISOString()));
      let result = await api.stopGate()
        .catch(err => commit('addError', err));
      dispatch('loadRequestHistory', api);
      return result?.success;
    },
    async closeGate({ dispatch, commit }: any, { api, silent }: any) {
      commit('addRequestHistory', new RequestHistoryItem(-1, 'Close', true, '', new Date().toISOString()));
      let result = await api.closeGate(silent);
      dispatch('loadRequestHistory', api)
      return result?.success;
    },
    async cycleGate({ dispatch, commit }: any, { api, duration }: any) {
      commit('addRequestHistory', new RequestHistoryItem(-1, 'Cycle ' + duration.toString() + " sec", true, '', new Date().toISOString()));
      let result = await (api as GateApi).cycleGate((duration as number));
      dispatch('loadRequestHistory', api);
      return result?.success;
    }
  },
  modules: {
  }
})
