import TimeHelpers from "@/helpers/timeHelpers";
import StateHistoryItem from "@/api/StateHistoryItem";
import RequestHistoryItem from "@/api/RequestHistoryItem";

export default class EventHistoryItem {
    public id: number;
    public success: boolean;
    public date: Date;
    public message: string;
    public error: string | null;

    constructor(id: number, success: boolean, date: Date, message: string, error: string | null) {
        this.id = id;
        this.success = success;
        this.date = date;
        this.message = message;
        this.error = error;
    }

    public static buildFromStateItem(item: StateHistoryItem): EventHistoryItem {
        return new EventHistoryItem(
            item.id, true, TimeHelpers.toDate(item.date),
            `Gate is ${item.state}`, null);
    }

    public static buildFromRequestItem(item: RequestHistoryItem): EventHistoryItem {
        return new EventHistoryItem(
            item.id, item.sent, TimeHelpers.toDate(item.date),
            `Request Gate ${item.request}`, `${!!item.error ? item.error : null}`);
    }
}
