export default class RequestHistoryItem {
    public id: number;
    public request: string;
    public sent: boolean;
    public error: string;
    public date: string;
    
    constructor(id: number, request: string, sent: boolean, error: string, date: string) {
        this.id = id;
        this.request = request;
        this.sent = sent;
        this.error = error;
        this.date = date;
    }
}
