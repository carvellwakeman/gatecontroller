export default class PollingHistoryItem {
    public id: number;
    public sent: boolean;
    public message: string;
    public retries: number | null;
    public rssiTx: number | null;
    public rssiRx: number | null;
    public date: string;
    
    constructor(id: number, sent: boolean, message: string, retries: number | null, rssiTx: number | null, rssiRx: number | null, date: string) {
        this.id = id;
        this.sent = sent;
        this.message = message;
        this.retries = retries;
        this.rssiTx = rssiTx;
        this.rssiRx = rssiRx;
        this.date = date;
    }
}
