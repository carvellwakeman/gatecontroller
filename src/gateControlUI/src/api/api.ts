import axios, { AxiosRequestConfig } from 'axios';
import RequestHistoryItem from './RequestHistoryItem';
import StateHistoryItem from './StateHistoryItem';
import UserModel from './UserModel';
// @ts-ignore
import { baseURL } from '@/app.config';
import ApiConfig from './ApiConfig';
import Result from './Result';
import AppError from './AppError';
import PollingHistoryItem from './PollingHistoryItem';
import SystemConfigurationModel from './SystemConfigurationModel';

console.log("baseURL")
console.log(baseURL)

export default class GateApi {

    constructor(){
        axios.defaults.validateStatus = (status: number) => {
            return (status >= 200 && status < 300);
        }
    }

    public getBasicAuthConfig(username: string, password: string) {
        return {
            auth: { username, password },
            timeout: 10000
        };
    }
    
    public login(username: string, password: string): Promise<string | null> {
        return axios
            .post(baseURL + "/login", {}, this.getBasicAuthConfig(username, password))
            // This is still getting hit for a 401...
            .then(resp => {
                return Promise.resolve(String(resp.data));
            })
            .catch(err => {
                return Promise.reject(err);
            });
    }

    public changePassword(password: string): Promise<void> {
        return axios
            .put(baseURL + "/changePassword", { "newPassword": password })
            .then(resp => {
                return Promise.resolve();
            })
            .catch(err => {
                console.log(err);
                return Promise.reject(err);
            });
    }

    public appConfig(): Promise<ApiConfig | null> {
        return axios
            .get(baseURL + "/config")
            .then(resp => {
                return resp.data;
            })
            .catch(err => {
                console.log(err);
                throw AppError.buildFrom(err);
            });
    }

    public metaLogs(): Promise<Blob> {
        return axios
            .get(baseURL + "/meta/logs", {responseType: 'blob'})
            .then(resp => {
                return new Blob([resp.data]);
            })
            .catch(err => {
                console.log(err);
                throw AppError.buildFrom(err);
            });
    }

    public metaGetDatabaseVersion(): Promise<number | null> {
        return axios
            .get(baseURL + "/meta/databaseVersion")
            .then(resp => {
              return resp.data
            })
            .catch(err => {
                console.log(err);
                throw AppError.buildFrom(err);
            });
    }

    public metaSetDatabaseVersion(version: number): Promise<void> {
        return axios
            .post(baseURL + "/meta/databaseVersion", {'version': version})
            .then(_ => {})
            .catch(err => {
                console.log(err);
                throw AppError.buildFrom(err);
            });
    }

    public metaShutdown(): Promise<string | null> {
        return axios
            .post(baseURL + "/meta/shutdown", {})
            .then(resp => {
                return String(resp.data.toLowerCase());
            })
            .catch(err => {
                console.log(err);
                throw AppError.buildFrom(err);
            });
    }

    
    public metaReboot(): Promise<string | null> {
        return axios
            .post(baseURL + "/meta/reboot", {})
            .then(resp => {
                return String(resp.data.toLowerCase());
            })
            .catch(err => {
                console.log(err);
                throw AppError.buildFrom(err);
            });
    }

    public getRequestHistory(): Promise<RequestHistoryItem[]> {
        return axios
            .get(baseURL + "/gate/request/history")
            .then(resp => {
                return resp.data.history;
            })
            .catch(err => {
                console.log(err);
                throw AppError.buildFrom(err);
            });
    }

    public getStateHistory(): Promise<StateHistoryItem[]> {
        return axios
            .get(baseURL + "/gate/state/history")
            .then(resp => {
                return resp.data.history;
            })
            .catch(err => {
                console.log(err);
                throw AppError.buildFrom(err);
            });
    }

    public getPollingHistory(): Promise<PollingHistoryItem[]> {
        return axios
            .get(baseURL + "/gate/polling/history")
            .then(resp => {
                return resp.data.history;
            })
            .catch(err => {
                console.log(err);
                throw AppError.buildFrom(err);
            });
    }

    public deleteStateHistory(): Promise<void> {
        return axios
            .delete(baseURL + "/gate/history")
            .then(resp => console.log(resp))
            .catch(err => {
                console.log(err)
                throw AppError.buildFrom(err);
            });
    }

    public getCurrentState(): Promise<Result | null> {
        return axios
            .get(baseURL + "/gate/state")
            .then(resp => {
                return resp.data;
            })
            .catch(err => {
                console.log(err);
                throw AppError.buildFrom(err);
            });
    }

    public openGate(silent: boolean): Promise<Result | null>  {
        return axios
            .post(baseURL + "/gate/open", {}, {params: { s: silent }})
            .then(resp => {
                return resp.data;
            })
            .catch(err => {
                console.log(err);
                throw AppError.buildFrom(err);
            });
    }

    public stopGate(): Promise<Result | null>  {
        return axios
            .post(baseURL + "/gate/stop", {})
            .then(resp => {
                return resp.data;
            })
            .catch(err => {
                console.log(err);
                throw AppError.buildFrom(err);
            });
    }

    public closeGate(silent: boolean): Promise<Result | null>  {
        return axios
            .post(baseURL + "/gate/close", {}, {params: { s: silent }})
            .then(resp => {
                return resp.data;
            })
            .catch(err => {
                console.log(err);
                throw AppError.buildFrom(err);
            });
    }

    public cycleGate(duration: number): Promise<Result | null> {
        return axios
        .post(baseURL + "/gate/cycle?d=" + duration.toString(), {})
        .then(resp => {
            return resp.data;
        })
        .catch(err => {
            console.log(err);
            throw AppError.buildFrom(err);
        });
    }

    public createUser(user: UserModel): Promise<void> {
        return axios
        .post(baseURL + "/users", JSON.stringify(user))
        .then(resp => {
        })
        .catch(err => {
            console.log(err);
            throw AppError.buildFrom(err);
        });
    }

    public getUsers(): Promise<UserModel[]> {
        return axios
            .get(baseURL + "/users")
            .then(resp => {
                return resp.data.users;
            })
            .catch(err => {
                console.log(err);
                throw AppError.buildFrom(err);
            });
    }

    public deleteUser(user: UserModel): Promise<void> {
        return axios
            .delete(baseURL + "/users/" + user.username)
            .then(resp => console.log(resp))
            .catch(err => {
                console.log(err)
                throw AppError.buildFrom(err);
            });
    }

    public saveSystemConfiguration(configuration: SystemConfigurationModel): Promise<void> {
        return axios
        .post(baseURL + "/configuration", JSON.stringify(configuration))
        .then(resp => {
        })
        .catch(err => {
            console.log(err);
            throw AppError.buildFrom(err);
        });
    }

    public getSystemConfigurations(): Promise<SystemConfigurationModel[]> {
        return axios
            .get(baseURL + "/configuration")
            .then(resp => {
                return resp.data.configuration;
            })
            .catch(err => {
                console.log(err);
                throw AppError.buildFrom(err);
            });
    }

    public deleteSystemConfiguration(configuration: SystemConfigurationModel): Promise<void> {
        return axios
            .delete(baseURL + "/configuration?systemConfigurationType=" + configuration.systemConfigurationType)
            .then(resp => console.log(resp))
            .catch(err => {
                console.log(err)
                throw AppError.buildFrom(err);
            });
    }
}
