export default class UserModel {
    public username: string;
    public role: string;
    public expiration: Date | null;
    public dateCreated: Date;

    constructor(username: string, role: string, expiration: Date | null) {
        this.username = username;
        this.role = role;
        this.expiration = expiration;
        this.dateCreated = new Date();
    }
}
