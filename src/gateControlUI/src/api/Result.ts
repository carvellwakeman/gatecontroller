export default class Result {
    public success: boolean;
    public addr: string;
    public message: string;
    public state: string;

    constructor(success: boolean, addr: string, message: string, state: string) {
        this.success = success;
        this.addr = addr;
        this.message = message;
        this.state = state;
    }
}
