export default class StateHistoryItem {
    public id: number;
    public addr: String;
    public state: String;
    public date: String;

    constructor(id: number, addr: String, state: String, date: String) {
        this.id = id;
        this.addr = addr;
        this.state = state;
        this.date = date;
    }
}
