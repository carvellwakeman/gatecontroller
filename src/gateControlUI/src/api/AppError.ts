export default class AppError {
    public timestamp: Date;
    public message: string;
    public inner: any | null;

    public static buildFrom(error: any) {
        return new AppError(
            new Date(),
            error.message,
            error.response.data
        );
    }

    constructor(timestamp: Date, message: string, inner: any | null) {
        this.timestamp = timestamp;
        this.message = message;
        this.inner = inner;
    }
}
