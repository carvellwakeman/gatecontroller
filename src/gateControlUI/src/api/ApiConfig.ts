export default class ApiConfig {
    public version: String;
    
    constructor(version: String) {
        this.version = version;;
    }
}
