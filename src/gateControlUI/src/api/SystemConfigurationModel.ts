export default class SystemConfigurationModel {
    public id: number | null;
    public systemConfigurationType: string;
    public value: string | null;
    public description: string | null;
    public dateCreated: Date;

    constructor(id: number | null, systemConfigurationType: string, value: string | null, description: string | null, dateCreated: Date) {
        this.id = id;
        this.systemConfigurationType = systemConfigurationType;
        this.value = value;
        this.description = description;
        this.dateCreated = dateCreated;
    }
}
