export default class TextHelpers {
    public static formatState(state: String) {
        if (state.toLowerCase() == 'moving') { return "Moving"; }
        if (state.toLowerCase() == 'open') { return "Open"; }
        if (state.toLowerCase() == 'closed') { return "Closed"; }
        return "Unknown";
    }
}