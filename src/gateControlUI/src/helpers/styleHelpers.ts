export default class StyleHelpers {


  public static stateTextColor(state: String) {
    if (state.toLowerCase() === 'open') { return 'green--text text--darken-2'; }
    if (state.toLowerCase() === 'moving') { return 'orange--text text--darken-2'; }
    if (state.toLowerCase() === 'closed') { return 'red--text text--darken-2'; }
    return 'blue--text text--darken-4';
  }

  public static stateIcon(state: String) {
    if (state.toLowerCase() === 'open') { return 'mdi-gate-open'; }
    if (state.toLowerCase() === 'moving') { return 'mdi-gate-arrow-right'; }
    if (state.toLowerCase() === 'closed') { return 'mdi-gate'; }
    return 'mdi-cloud-question';
  }

  public static stateIconColor(state: String) {
    if (state.toLowerCase() === 'open') { return 'green darken-2'; }
    if (state.toLowerCase() === 'moving') { return 'orange darken-2'; }
    if (state.toLowerCase() === 'closed') { return 'red darken-2'; }
    return 'blue darken-4';
  }

  public static stateTextFormat(state: String) {
    if (state.length < 2) { return state; }

    return state[0].toUpperCase() + state.slice(1).toLowerCase();
  }
}