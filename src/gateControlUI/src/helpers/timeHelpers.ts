export default class TimeHelpers {
    public static toDate(date: String): Date {
        return new Date(Date.parse(date.toString()))
    }

    public static millisecondDiff(r: Date | null) {
        if (!r) {
            return 0;
        }

        let l = Date.now(); // now

        return l - r.getTime()
    }

    public static buildLastUpdatedText(date: Date) {
        let diffMs = TimeHelpers.millisecondDiff(date);

        let diffSec = Math.floor(diffMs / 1000);
        let diffMin = Math.floor(diffMs / 1000 / 60);
        let diffHr = Math.floor(diffMs / 1000 / 60 / 60);

        if (diffSec < 1) {
            return 'now';
        }

        if (diffMin < 1) {
            return diffSec.toString() + ' Second' + (diffSec > 1 ? 's' : '') + ' ago';
        }

        if (diffHr < 1) {
            return diffMin.toString() + ' Minute' + (diffMin > 1 ? 's' : '') + ' ago';
        }

        return diffHr.toString() + ' Hour' + (diffHr > 1 ? 's' : '') + ' ago';
    }
}