import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import Settings from '../views/Settings.vue'
import store from '@/store/store'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/settings',
    name: 'settings',
    component: Settings,
    beforeEnter: (to, from, next) => {
      if (store.getters.isUserRole || store.getters.isAdminRole)
      {
        next();
      } else {
        next('/');
      }
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
