npm install
npm run build

# Link site to serve from Nginx
sudo rm -rf /var/www/html
sudo rm -rf $(realpath ../dist/index.nginx-debian.html)
sudo ln -sv $(realpath ../dist) /var/www/html
sudo ln -s $(realpath ../dist/index.html) $(realpath ../dist/index.nginx-debian.html)
