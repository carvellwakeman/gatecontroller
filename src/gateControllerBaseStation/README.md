### Dependencies
Python 3.10

### Installing
python3 -m pip install -r ./requirements.txt


### Run

## Release mode
python3 src/app.py --release --usb /dev/ttyUSB0 --data gate.db --recv_radio_addr 0013A200AAAAAAAA

## Dev/Mock mode
# Radio USB is mocked in dev mode
# OR usb can be COM port if running locally (--usb COM5)
# --data parameter optional, will be in memory db when not supplied
python3 src/app.py --dev


### Interaction
http://localhost:2300


### Tests
cd src/tests
python -m pytest

### Debug
docker exec -it gatecontroller bash
