from abstractRadio import AbstractRadio
from xbeeRadio import XbeeRadio
from mockRadio import MockRadio
from environment import isDev, radioPort
from datetime import timedelta
import threading

'''
Returns an instance of AbstractRadio. based on environment
'''
class RadioWorker():
    _instance = None
    _thread = None

    def __init__(self):
        raise RuntimeError('Call instance() instead')

    @classmethod
    def instance(cls):
        if cls._instance is None:
            if radioPort == "MOCKED":
                cls._instance = MockRadio()
            else:
                cls._instance = XbeeRadio() #PassThroughRadio()
            #  = cls.__new__(cls)
            # Put any initialization here.
        return cls._instance

    @classmethod
    def run(cls):
        cls._thread = threading.Thread(target=cls.instance().listen)
        cls._thread.setDaemon(True)
        cls._thread.start()

    @classmethod
    def stop(cls):
        cls._instance.closeRadio()
        cls._thread.join()

    @classmethod
    def sendBroadcast(cls, command, stop=True):
        return cls._instance.sendBroadcast(command, stop)
    
    @classmethod
    def sendToRemote(cls, command, stop=True):
        return cls._instance.sendToRemote(command, stop)
    
    @classmethod
    def runLocalCommand(cls, command, type='AT'):
        return cls._instance.runLocalCommand(command, type)
    
    @classmethod
    def runRemoteCommand(cls, command, type='AT'):
        return cls._instance.runRemoteCommand(command, type)


