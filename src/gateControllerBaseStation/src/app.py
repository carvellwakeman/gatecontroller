################################################################################
# Gate Controller Master V2 (sends and receives commands) - Zach L
################################################################################
import eventlet
import flask
eventlet.monkey_patch(thread=False)


import sys
from flask import Flask
from flask import request
from flask_cors import CORS
from flask_json import FlaskJSON
from flask_socketio import SocketIO, emit
import signal
from environment import port, host, redisConnString
from radioWorker import RadioWorker
from connectivityWorker import ConnectivityWorker
from api import appController, currentStateController, gateControlController, historyController, metaController, usersController
from auth import authController
from stateChangeHandler import stateChangeHandler
from programKilled import signal_handler, ProgramKilled
from fileLogger import logger

##### Flask #####
flask_app = Flask(__name__)
flask_app.register_blueprint(appController)
flask_app.register_blueprint(currentStateController)
flask_app.register_blueprint(gateControlController)
flask_app.register_blueprint(historyController)
flask_app.register_blueprint(metaController)
flask_app.register_blueprint(authController)
flask_app.register_blueprint(usersController)

##### Plugins #####
FlaskJSON(flask_app)
CORS(flask_app)

################ Middleware ##############
# flask_app.wsgi_app = LoggingMiddleware(flask_app.wsgi_app)

@flask_app.after_request
def after_request(response):
    logger.debug('%s %s %s %s %s', request.remote_addr, request.method, request.scheme, request.full_path, response.status)
    return response

@flask_app.errorhandler(Exception)
def exceptions(e):
    tb = e.traceback.format_exc() if getattr(e, 'traceback', None) != None else str(e)
    logger.error('%s %s %s %s 5xx INTERNAL SERVER ERROR\n%s', request.remote_addr, request.method, request.scheme, request.full_path, tb)
    return tb, 500, {"Access-Control-Allow-Origin": "*"}


# The Application
def main():
    # logger.basicConfig(filename='gateControllerBaseStation.log', encoding='utf-8', level=logger.DEBUG, format='%(levelname)s:[%(asctime)s]:%(message)s', force=True)
    logger.info('[App] Starting')

    # Socket IO
    socketio = SocketIO(flask_app, message_queue=redisConnString(), cors_allowed_origins='*', async_mode="eventlet")#  ,logger=True, engineio_logger=True
    logger.info('[APP] SocketIO started using message queue: ' + str(redisConnString()))

    # Catch signals
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGINT, signal_handler)

    try:
        # Add socketio to stateChangeHandler
        stateChangeHandler.setSocketio(socketio)

        # Add stateChangeHandler to radio listeners
        RadioWorker.instance().addMessageListener(stateChangeHandler.handleStateChange)
        RadioWorker.run()

        # Pings radio at a regular interval
        ConnectivityWorker.run()

        # Serve API
        socketio.run(flask_app, port=port, host=host, log_output=True)
        # flask_app.run(host=host, port=port, debug=True, use_reloader=False)
        
    except Exception as ex:
        logger.info("[App] Halting due to " + str(ex))
        socketio.stop()
        RadioWorker.stop()


###### Websockets #####
# @socketio.on('connect')
# def socket_connect():
#     logger.debug("[SocketIO] Client Connected.")
#     # emit('myEvent', {'data': 1})


# @socketio.on('my_event')
# def test_message(message):
#     emit('my response', {'data': message['data']})

# Start
if __name__ == "__main__":
    main()
