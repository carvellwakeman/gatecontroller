from abstractRadio import AbstractRadio
from result import Result
import time
from digi.xbee.devices import XBeeDevice, RemoteXBeeDevice
# from digi.xbee.models.message import XBeeMessage
from fileLogger import logger
from random import Random

MOCK_ADDR = '0000000000000000'
class MockRadio(AbstractRadio):
    def __init__(self):
        super().__init__()
        self.localDevice = XBeeDevice(port="MOCKED", baud_rate=1)
        self.remoteDevice = RemoteXBeeDevice(self.localDevice)
        self.lastState = ['o','m','c'][Random().randint(0,2)]
        self.mockQueue = []
        logger.info("[Radio] Using MOCKED Radio")

    def listen(self):
        while (True):
            if len(self.mockQueue) > 0:
                q = self.mockQueue.pop()
                self.informListeners(MOCK_ADDR, q)

    # 0 = do nothing, 1 = open gate, 2 = stop gate, 3 = close gate, >3 = wait seconds
    def sendBroadcast(self, command, stop=True):
        # time.sleep(Random(time.time()).randint(1,3))
        if (command == 1):
            # return Result(False, 'MOCK', 'Some fake failure happened')
            logger.info("[MOCK RADIO] Opening")
            self.lastState = 'o'
            self.mockQueue.append('o')
        elif (command == 2):
            logger.info("[MOCK RADIO] Stopped")
            self.lastState = 'm'
            self.mockQueue.append('m')
        elif (command == 3):
            logger.info("[MOCK RADIO] Closing")
            self.lastState = 'c'
            self.mockQueue.append('c')
        elif (command == 4):
            logger.info("[MOCK RADIO] Get State " + self.lastState)
            self.informListeners(MOCK_ADDR, self.lastState)
            if (Random(time.time()).randint(1,2) == 2):
                return Result(False, 'MOCK', 'Mock Error')
            return Result(True, 'MOCK', self.lastState)

        return Result(True, 'MOCK', '')
    
    # 0 = do nothing, 1 = open gate, 2 = stop gate, 3 = close gate, >3 = wait seconds
    def sendToRemote(self, command, stop=True):
        return self.sendBroadcast(command, stop)
