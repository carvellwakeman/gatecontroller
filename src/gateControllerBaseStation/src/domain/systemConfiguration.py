from datetime import datetime
class SystemConfiguration:
    def __init__(self, id, systemConfigurationType, systemConfigurationTypeDescription, value, dateCreated):
        self.id = id
        self.systemConfigurationType = systemConfigurationType
        self.description = systemConfigurationTypeDescription
        self.value = value
        self.dateCreated = dateCreated
    
    def toJson(self):
        return {
            'id': self.id,
            'systemConfigurationType': self.systemConfigurationType,
            'description': self.description,
            'value': self.value,
            'dateCreated': self. dateCreated
        }
