from datetime import datetime
class User:
    def __init__(self, id, username, hashedPassword, role, expiration, dateCreated):
        self.id = id
        self.username = username
        self.hashedPassword = hashedPassword
        self.role = role
        self.expiration = expiration
        self.dateCreated = dateCreated
    
    def toJson(self):
        return {
            'username': self.username,
            'role': self.role,
            'expiration': self.expiration,
            'dateCreated': self. dateCreated
        }
    
    def isExpired(self):
        if self.expiration and datetime.utcnow() > datetime.strptime(self.expiration, '%Y-%m-%dT%H:%M:%S.%fZ'):
            return True
        return False