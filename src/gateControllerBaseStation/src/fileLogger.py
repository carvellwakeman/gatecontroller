from flask.logging import default_handler
import logging
from logging.handlers import RotatingFileHandler

# Logging
f = logging.Formatter('[%(asctime)s] %(process)d:%(thread)d %(levelname)s %(message)s')

logger = logging.getLogger('tdm')
logger.setLevel(logging.DEBUG)

handler = RotatingFileHandler('gateControllerBaseStation.log', maxBytes=100000, backupCount=3)
handler.setFormatter(f)
logger.addHandler(handler)

default_handler.setFormatter(f)
logger.addHandler(default_handler)


def getLogFilePath():
    for handler in logger.handlers:
        if hasattr(handler, "baseFilename"):
            return getattr(handler, 'baseFilename')
    return None