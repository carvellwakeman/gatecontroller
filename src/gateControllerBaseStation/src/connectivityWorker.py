from datetime import datetime, timedelta
from result import Result
from radioWorker import RadioWorker
import threading
from fileLogger import logger
from repository import repository
import time
import sys

'''
Polls the radio on a regular interval, records results to database
'''
class ConnectivityWorker():
    _thread = None

    def __init__(self):
        raise RuntimeError('Call instance() instead')

    @classmethod
    def run(cls):
        cls._thread = threading.Thread(target=cls.poll)
        cls._thread.setDaemon(True)
        cls._thread.start()

    @classmethod
    def stop(cls):
        cls._thread.join()

    def poll():
        nextRun = datetime.now()
        lastRun = datetime.now()

        while(True):
            time.sleep(1)

            pollingEnabledSysConfig = repository.getSystemConfiguration('polling_enabled')
            pollingEnabled = (pollingEnabledSysConfig and pollingEnabledSysConfig.value and pollingEnabledSysConfig.value.lower() == 'true') or False        

            # Skip while disabled
            if (not pollingEnabled):
                continue

            now = datetime.now()

            pollingIntervalSysConfig = repository.getSystemConfiguration('polling_interval')
            pollingInterval = pollingIntervalSysConfig and pollingIntervalSysConfig.value or 3600
            pollingIntervalTimeDelta = timedelta(seconds=int(pollingInterval))

            # If polling interval changes while we're waiting, adjust next run time
            if (lastRun + pollingIntervalTimeDelta != nextRun):
                nextRun = now - timedelta(seconds=1) # put next run in the past

            # NextRun has passed
            if (now < nextRun):
                continue
            
            lastRun = now
            nextRun = now + pollingIntervalTimeDelta

            result = RadioWorker.sendToRemote(4)

            rssi_local = RadioWorker.runLocalCommand('DB', 'AT')
            rssi_remote = RadioWorker.runRemoteCommand('DB', 'AT')

            rssi_local_strength = ord(rssi_local) * -1 if rssi_local else 0
            rssi_remote_strength = ord(rssi_remote) * -1 if rssi_remote else 0

            repository.insertPollingtHistory(result, rssi_local_strength, rssi_remote_strength)
            logger.info("[Connectivity] Polled radio: " + str(result.success) + " (rssi_L " + str(rssi_local_strength) + ", rssi_R " + str(rssi_remote_strength) + ")")

            