def byteStateToString(state):
    if (state == 'm'): return "Moving"
    if (state == 'o'): return "Open"
    if (state == 'c'): return "Closed"
    return "Unknown"


def byteRequestToString(request):
        if (request == 's'): return "Stop"
        if (request == 'o'): return "Open"
        if (request == 'c'): return "Close"
        if (request == 'w'): return "Wait"
        if (request.startswith('y')): return "Cycle " + request[1::] + " sec"
        return "Unknown"

def stringToByteState(state):
    if (state.lower() == 'closed'): return 'c'
    if (state.lower() == 'open'): return 'o'
    if (state.lower() == 'moving'): return 'm'
    return "u"

def isCloseToOpen(oldState, newState):
    if (oldState == 'c' and newState == 'm'): return True
    if (oldState == 'c' and newState == 'o'): return True
    return False

def isOpenToClose(oldState, newState):
    if (oldState == 'o' and newState == 'm'): return True
    if (oldState == 'o' and newState == 'c'): return True
    return False