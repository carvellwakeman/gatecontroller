import sys
import os
from fileLogger import logger
import pathlib

arg_environment = 'release' if '--release' in [arg.lower() for arg in sys.argv] else 'dev'
arg_radioPort = sys.argv[sys.argv.index('--usb') + 1] if '--usb' in sys.argv else 'MOCKED'
arg_databaseFilepath = sys.argv[sys.argv.index('--data') + 1] if '--data' in sys.argv else None
arg_receiverRadioAddress = sys.argv[sys.argv.index('--recv_radio_addr') + 1] if '--recv_radio_addr' in sys.argv else None # Default to broadcast
arg_redisUrl = sys.argv[sys.argv.index('--redis') + 1] if '--redis' in sys.argv else None
port = 2300
host = '0.0.0.0'

apiVersion = os.getenv('API_VERSION', '0.0')
environment = os.getenv('RUN_ENV', arg_environment)
radioPort = os.getenv('USB_PORT', arg_radioPort)
databaseFilepath = os.getenv('DATABASE', arg_databaseFilepath)
redisUrl = os.getenv('REDIS', arg_redisUrl)
receiverRadioAddress = os.getenv('RECV_RADIO_ADDR', arg_receiverRadioAddress)

soundNotifyWindowStart = 20 #8:00pm
soundNotifyWindowEnd = 6 #5:59am

# Environment
def isDev():
    return environment == 'dev'

def isRelease():
    return environment == 'release'

def redisConnString():
    return redisUrl # None is expected and okay here for local dev

def getApiVersion():
    return apiVersion

def dbConnString():
    if databaseFilepath == None:
        return { 'database': ':memory:', 'check_same_thread': False }
    else:
        absPath = os.path.abspath(databaseFilepath)
        pathWithoutFilename = os.sep.join(absPath.split(os.sep)[:-1])
        pathlib.Path(pathWithoutFilename).mkdir(parents=True, exist_ok=True)
        return { 'database': databaseFilepath, 'check_same_thread': False }      

def dbFirstRun():
    if databaseFilepath == None:
        return True
    else:
        firstRun = (not os.path.isfile(databaseFilepath)) or fileEmpty(databaseFilepath)
    return firstRun

def fileEmpty(filepath):
    try:
        return os.path.getsize(filepath) <= 0
    except:
        return True

logger.info("[Environment] Running in " + environment + " mode")