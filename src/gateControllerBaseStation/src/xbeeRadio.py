from abstractRadio import AbstractRadio
from result import Result
from digi.xbee.devices import XBeeDevice, RemoteXBeeDevice, XBee64BitAddress, XBee16BitAddress
from digi.xbee.models.message import XBeeMessage
from digi.xbee.models.atcomm import ATStringCommand
from digi.xbee.exception import *
import time
from datetime import datetime
from environment import radioPort, receiverRadioAddress
from programKilled import ProgramKilled
from fileLogger import logger

class XbeeRadio(AbstractRadio):
    def __init__(self):
        self.broadcastAddr = "000000000000FFFF"
        super().__init__()

    def openRadio(self):
        logger.info("[Radio] Opening Xbee Radio at " + radioPort)
        logger.info("[Radio] Remote Xbee is " + str(receiverRadioAddress))
        if self.local_xbee is None or not self.local_xbee.is_open():
            try:
                self.local_xbee = XBeeDevice(radioPort, 9600)
                self.local_xbee.open()
                # logger.info("[Radio] Opened Xbee Radio (" + str(self.remote_xbee.get_64bit_addr()) + ") at " + radioPort + " in mode '" + self.local_xbee.operating_mode.description + "'")
                # self.local_xbee.set_sync_ops_timeout(8)
                # self.local_xbee.add_data_received_callback(self.informListeners)
            except Exception as ex:
                logger.exception("[Radio] Could not open radio at " + radioPort + " because " + str(ex))
                raise ProgramKilled
            
    def closeRadio(self):
        logger.info("[Radio] Closing Xbee Radio at " + radioPort)
        self.listening = False
        if self.local_xbee is not None and self.local_xbee.is_open():
            self.local_xbee.close()

    def listen(self):
        self.local_xbee = None
        self.listening = True
        self.openRadio()
        self.local_xbee.add_data_received_callback(self.receiveMessage)

    def receiveMessage(self, message):
        # Sender
        address = str(message.remote_device.get_64bit_addr())

        # Get latest state
        data = message.data.decode("utf8")

        logger.info("[Radio] Receive Packet '" + data  + "' from '" + address + "'")

        self.informListeners(address, data)

        return (address, data) 

    # 0 = do nothing, 1 = open gate, 2 = stop gate, 3 = close gate, 4 = get state, >3 = wait seconds
    def sendBroadcast(self, command, stop=True):
        try:
            packet = str(command) + ',' + str(1 if stop else 0) # "encoding"
            logger.info("[Radio] Sending Packet: " + packet)
            
            response = self.local_xbee.send_data_broadcast(packet)

            logger.info("[Radio] Packet " + packet + " Sent " + response.transmit_status.description + " w/ "+ str(response.transmit_retry_count) +" retries")

            return Result(response.transmit_status.code == 0, self.broadcastAddr, '', response.transmit_retry_count)
        except TimeoutException as ex:
            logger.exception("[Radio] Send Failed (timeout): " + str(ex))
            return Result(False, self.broadcastAddr, str(ex))
        except InvalidOperatingModeException as ex:
            logger.exception("[Radio] Send Failed: (operating mode incorrect) " + str(ex))
            return Result(False, self.broadcastAddr, str(ex))
        except TransmitException as ex:
            logger.exception("[Radio] Send Failed (transmit fail): " + str(ex))
            return Result(False, self.broadcastAddr, str(ex))
        except XBeeException as ex:
            logger.exception("[Radio] Send Failed (error writing to radio): " + str(ex))
            return Result(False, self.broadcastAddr, str(ex))
        except Exception as ex:
            logger.exception("[Radio] Send Failed (other): " + str(ex))
            return Result(False, self.broadcastAddr, str(ex))

    # 0 = do nothing, 1 = open gate, 2 = stop gate, 3 = close gate, 4 = get state, >3 = wait seconds
    def sendToRemote(self, command, stop=True):
        try:
            if (receiverRadioAddress is None):
                raise Exception("Remote radio address not configured, cannot direct send.")
        
            packet = str(command) + ',' + str(1 if stop else 0) # "encoding"
            logger.info("[Radio] Sending Packet (blocking): " + packet)
            
            self.remote_xbee = RemoteXBeeDevice(self.local_xbee, XBee64BitAddress.from_hex_string(receiverRadioAddress))
            response = self.local_xbee.send_data(self.remote_xbee, packet)

            logger.info("[Radio] Packet " + packet + " Sent " + response.transmit_status.description + " w/ "+ str(response.transmit_retry_count) +" retries")

            return Result(response.transmit_status.code == 0, receiverRadioAddress, None, response.transmit_retry_count)
        except TimeoutException as ex:
            logger.exception("[Radio] Send Failed (timeout): " + str(ex))
            return Result(False, str(self.remote_xbee.get_64bit_addr()), str(ex))
        except InvalidOperatingModeException as ex:
            logger.exception("[Radio] Send Failed: (operating mode incorrect) " + str(ex))
            return Result(False, str(self.remote_xbee.get_64bit_addr()), str(ex))
        except TransmitException as ex:
            logger.exception("[Radio] Send Failed (transmit fail): " + str(ex))
            return Result(False, str(self.remote_xbee.get_64bit_addr()), str(ex))
        except XBeeException as ex:
            logger.exception("[Radio] Send Failed (error writing to radio): " + str(ex))
            return Result(False, str(self.remote_xbee.get_64bit_addr()), str(ex))
        except Exception as ex:
            logger.exception("[Radio] Send Failed (other): " + str(ex))
            return Result(False, str(self.remote_xbee.get_64bit_addr()), str(ex))

    def runLocalCommand(self, command, type='AT'):
        try:
            if type == 'AT':
                response = self.local_xbee.get_parameter(command)
                return response.decode('utf8')
            else:
                raise Exception("Unsupported local command type " + type)
        except Exception as ex:
            return None

    def runRemoteCommand(self, command, type='AT'):
        try:
            if type == 'AT':
                response = self.remote_xbee.get_parameter(command)
                return response.decode('utf8')
            else:
                raise Exception("Unsupported remote command type " + type)
        except Exception as ex:
            return None

    def getMode(self):
        if self.local_xbee is not None and self.local_xbee.is_open():
            return self.local_xbee.operating_mode.description
        return None
