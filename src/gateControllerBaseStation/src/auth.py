from flask import Blueprint, request
from flask_httpauth import HTTPTokenAuth
from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from repository import repository
from datetime import datetime
from base64 import b64decode

token_serializer = Serializer('secret key here', expires_in=2592000) # 30 days

flask_auth = HTTPTokenAuth('Bearer')

authController = Blueprint('auth', __name__, template_folder='templates')

################ Auth ##############
@authController.route('/login', methods=['POST'])
def login():
    # Pull from basic auth header
    authHeader = request.headers['authorization'] if 'authorization' in request.headers else None

    if authHeader is None:
        return "Access Denied", 401

    # Decode basic auth header
    encodedBasicAuth = authHeader.split(' ')[1]
    decodedBasicAuth = b64decode(encodedBasicAuth).decode("utf-8") 
    creds = decodedBasicAuth.split(":")
    username = creds[0].lower()
    password = creds[1]

    # Special case for no users
    if username == "admin" and password == "admin":
        users = repository.getUsers()
        if len(users) == 0:
            repository.insertUser("admin", "admin")

    user = repository.getUser(username)
    if user:
        if not user.hashedPassword:
            repository.updateUserPassword(username, generate_password_hash(password))
            user = repository.getUser(username)
        
        if user.isExpired():
            return "Your credentials have Expired", 403

        if check_password_hash(user.hashedPassword, password):
            token = token_serializer.dumps(user.toJson()).decode('utf-8')
            return token

    return "Username and Password do not match", 401

@authController.route('/changePassword', methods=['PUT'])
@flask_auth.login_required()
def changePassword():
    username = flask_auth.current_user()['username']

    json = request.get_json(force=True)
    newPassword = json['newPassword']

    user = repository.getUser(username)
    if not user:
        return "User not found", 404

    repository.updateUserPassword(username, generate_password_hash(newPassword))

    return "", 200

################ Flask HttpAuth overrides ################
@flask_auth.get_user_roles
def get_user_roles(user):
    # Comes from auth token claims
    return [user['role']]

@flask_auth.verify_token
def verify_token(token):
    if not token:
        return False

    try:
        data = token_serializer.loads(token)
    except:  # noqa: E722
        return False

    if 'username' in data:
        user = repository.getUser(data['username'])
        if user:
            if user.isExpired():
                return False
            return user.toJson()
    
    return False

@flask_auth.error_handler
def auth_error(status):
    return "Access Denied", status
