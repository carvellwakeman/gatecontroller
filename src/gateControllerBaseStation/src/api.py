import os
import subprocess
import time
from datetime import datetime, timedelta
from flask import Blueprint, request, send_from_directory
from flask_json import JsonError, json_response, as_json
from werkzeug.security import generate_password_hash, check_password_hash
from fileLogger import getLogFilePath
from domain.systemConfiguration import SystemConfiguration
from helpers import byteStateToString, byteRequestToString
from result import Result
from radioWorker import RadioWorker
from audio import audio
from environment import *
from repository import repository
from audio import audio
from auth import flask_auth

################# Routes #################
appController = Blueprint('app_info', __name__, template_folder='templates')
metaController = Blueprint('meta_control', __name__, template_folder='templates')
currentStateController = Blueprint('current_state', __name__, template_folder='templates')
gateControlController = Blueprint('gate_control', __name__, template_folder='templates')
historyController = Blueprint('history', __name__, template_folder='templates')
usersController = Blueprint('users', __name__, template_folder='templates')

## Root Info controller
@appController.route('/', methods=['GET'])
# @flask_auth.login_required
@as_json
def app_info():
    response = { 
        'Name': 'Gate Controller',
        'Version': 'TBD',
        'environment': environment,
        'user': flask_auth.current_user(),
        'Urls': [
            {'swagger':request.base_url + '/swagger'}
        ]
    }
    return response

## Config Controller
@appController.route('/config', methods=['GET'])
@as_json
def app_config():
    response = {
        'version': getApiVersion()
    }
    return response

## Meta Controller
@metaController.route('/meta/logs', methods=['GET'])
@flask_auth.login_required(role='admin')
def meta_logs():
    filePath = getLogFilePath()

    try:
        head_tail = os.path.split(filePath)
        return send_from_directory(head_tail[0], head_tail[1], as_attachment=True)
    except FileNotFoundError:
        'Logs not found',404

@metaController.route('/meta/databaseVersion', methods=['GET'])
@flask_auth.login_required(role='admin')
def meta_getdatabaseVersion():
    return str(repository.getDatabaseVersion())

@metaController.route('/meta/databaseVersion', methods=['POST'])
@flask_auth.login_required(role='admin')
def meta_setdatabaseVersion():
    json = request.get_json(force=True)
    version = json['version']
    repository.setDatabaseVersion(int(version))
    return 'Success',200

@metaController.route('/meta/shutdown', methods=['POST'])
@flask_auth.login_required(role='admin')
def meta_shutdown():
    subprocess.call(["shutdown", "+0"])
    return 'Shutting down'

@metaController.route('/meta/reboot', methods=['POST'])
@flask_auth.login_required(role=['admin', 'user'])
def meta_reboot():
    subprocess.call(["reboot"])
    return 'Rebooting'

@metaController.route('/test/sound', methods=['GET'])
@flask_auth.login_required(role='admin')
def gate_sound():
    audio.play('./sound/bells/double/shipbell1.wav')
    return 'Ding'


## State Controller
@currentStateController.route('/gate/state', methods=['GET'])
@flask_auth.login_required(role=['admin', 'user', 'guest'])
def gate_state():
    result = RadioWorker.sendBroadcast(4)
    time.sleep(1)
    state = repository.getMostRecentState()
    # if not result.sent:
    return { 'success': result.success, 'addr': result.addr, 'message': result.message, 'state': byteStateToString(state[2]) }

## Gate Controller
@gateControlController.route('/gate/open', methods=['POST'])
@flask_auth.login_required(role=['admin', 'user', 'guest'])
def gate_open():
    silent = request.args.get('s', default=False, type=lambda v: v.lower() == 'true')

    if silent:
        now = datetime.now()
        gateDelay = timedelta(seconds=14) # TODO Pull gate duration from config
        repository.insertNotificationScheduleOverride('alarm', 0, now, now + gateDelay)

    result = RadioWorker.sendBroadcast(1)
    repository.insertRequestHistory('o', result)

    return result.toJson()
    
@gateControlController.route('/gate/stop', methods=['POST'])
@flask_auth.login_required(role=['admin', 'user', 'guest'])
def gate_stop():
    result = RadioWorker.sendBroadcast(2)
    repository.insertRequestHistory('s', result)

    return result.toJson()

@gateControlController.route('/gate/close', methods=['POST'])
@flask_auth.login_required(role=['admin', 'user', 'guest'])
def gate_close():
    silent = request.args.get('s', default=False, type=lambda v: v.lower() == 'true')

    if silent:
        now = datetime.now()
        gateDelay = timedelta(seconds=14) # TODO Pull gate duration from config
        repository.insertNotificationScheduleOverride('alarm', 0, now, now + gateDelay)

    result = RadioWorker.sendBroadcast(3)
    repository.insertRequestHistory('c', result)

    return result.toJson()

@gateControlController.route('/gate/wait', methods=['POST'])
@flask_auth.login_required(role=['admin', 'user'])
def gate_wait():
    query = request.args.get('d')
    duration = 30 if query is None else query
    result = RadioWorker.sendBroadcast(duration, True)
    repository.insertRequestHistory('w', result)
    
    return result.toJson()

@gateControlController.route('/gate/cycle', methods=['POST'])
@flask_auth.login_required(role=['admin', 'user'])
def gate_cycle():
    query = request.args.get('d')
    duration = 30 if query is None else int(query)

    result1 = RadioWorker.sendBroadcast(1, False)
    result2 = RadioWorker.sendBroadcast(duration, False)
    result3 = RadioWorker.sendBroadcast(3, True)

    # hijack one of the three results
    result1.success = result1.success and result2.success and result3.success
    result1.message = (result1.message or "") + "_" + (result2.message or "") + "_" + (result3.message or "")
    repository.insertRequestHistory('y'+str(duration), result1)

    return result1.toJson()

## History
@historyController.route('/gate/state/history', methods=['GET'])
@flask_auth.login_required(role=['admin', 'user'])
@as_json
def gate_state_history():
    stateHistory = repository.getStateHistory()

    response = { 'history': [] }
    for h in stateHistory:
        response['history'].append({ 'id': h[0], 'addr': h[1], 'state': byteStateToString(h[2]), 'date': h[3] })
    
    return response

@historyController.route('/gate/request/history', methods=['GET'])
@flask_auth.login_required(role=['admin', 'user'])
@as_json
def gate_request_history():
    requestHistory = repository.getRequestHistory()

    response = { 'history': [] }
    for h in requestHistory:
        response['history'].append({ 'id': h[0], 'request': byteRequestToString(h[1]), 'sent': h[2], 'error': h[3], 'date': h[4] })
    
    return response

@historyController.route('/gate/polling/history', methods=['GET'])
@flask_auth.login_required(role=['admin'])
@as_json
def gate_polling_history():
    pollingHistory = repository.getPollingHistory()
    pollingHistory.reverse()

    response = { 'history': [] }
    for h in pollingHistory:
        response['history'].append({ 'id': h[0], 'sent': h[1], 'message': h[2], 'retries': h[3], 'rssiTx': h[4], 'rssiRx': h[5], 'date': h[6] })
    
    return response

@historyController.route('/gate/history', methods=['DELETE'])
@flask_auth.login_required(role=['admin', 'user'])
def gate_delete_history():
    repository.deleteAllHistory()
    return '', 204


## Users Controller
@usersController.route('/users', methods=['POST'])
@flask_auth.login_required(role='admin')
def create_user():
    json = request.get_json(force=True)
    username = json['username']

    existingUser = repository.getUser(username)
    if existingUser:
        return 'User already exists',409

    repository.insertUser(username, json['role'], json['expiration'] if 'expiration' in json else None)
    return '', 201

@usersController.route('/users/<username>', methods=['PUT'])
@flask_auth.login_required(role='admin')
def update_user():
    json = request.get_json(force=True)
    username = json['username']

    user = repository.getUser(username)
    if user:
        repository.updateUser(json['username'], json['role'], json['expiration'] if 'expiration' in json else None)
        return '', 204

    return '', 404

@usersController.route('/users', methods=['GET'])
@flask_auth.login_required(role=['admin', 'user'])
@as_json
def get_users():
    users = repository.getUsers()
    return {"users": [user.toJson() for user in users]}

@usersController.route('/users/<username>', methods=['DELETE'])
@flask_auth.login_required(role='admin')
def delete_user(username):
    # cannot delete yourself
    if username == flask_auth.current_user()['username']:
        return 'Cannot delete yourself', 400
    
    repository.deleteUser(username)
    
    return '', 204


## Configuration controller
@usersController.route('/configuration', methods=['POST'])
@flask_auth.login_required(role='admin')
def create_configuration():
    json = request.get_json(force=True)
    
    systemConfigurationType = json['systemConfigurationType']
    value = json['value']

    result = repository.SaveSystemConfiguration(systemConfigurationType, value)

    if not result[0]:
        return result[1], 400

    return '', 201

@usersController.route('/configuration', methods=['GET'])
@flask_auth.login_required(role=['admin', 'user'])
@as_json
def get_configurations():
    configurations = list(repository.getSystemConfigurations())
    configurationTypes = repository.getSystemConfigurationTypes()

    setConfigurationTypes = [cfg.systemConfigurationType for cfg in configurations]

    unsetConfigurations = [SystemConfiguration(-1, cfg_typ[1], cfg_typ[2], None, None) for cfg_typ in configurationTypes if cfg_typ[1] not in setConfigurationTypes]

    return {"configuration": [config.toJson() for config in configurations + unsetConfigurations]}

@usersController.route('/configuration', methods=['DELETE'])
@flask_auth.login_required(role=['admin', 'user'])
@as_json
def delete_configuration():
    systemConfigurationType = request.args.get('systemConfigurationType')

    result = repository.deleteSystemConfiguration(systemConfigurationType)
    if not result[0]:
        return result[1],400
    
    return '',204
