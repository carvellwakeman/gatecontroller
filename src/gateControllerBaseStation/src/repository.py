import os
import sqlite3
from datetime import datetime
from fileLogger import logger
from domain.user import User
from domain.systemConfiguration import SystemConfiguration
from environment import dbConnString, dbFirstRun

################# SQL META ###############
DATABASE_VERSION = 6
GET_DATABASE_VERSION = """PRAGMA user_version"""
SET_DATABASE_VERSION = """PRAGMA user_version = {v:d};"""

################# SQL MIGRATION #####################
CREATE_TABLE_RequestHistory = """
    CREATE TABLE RequestHistory (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        request TEXT,
        sent INTEGER,
        error TEXT,
        dateCreated timestamp
    );"""
CREATE_TABLE_StateHistory = """
    CREATE TABLE StateHistory (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        addr TEXT,
        state TEXT,
        dateCreated timestamp
    );"""
CREATE_TABLE_User = """
    CREATE TABLE User (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        username TEXT,
        hashedPassword TEXT,
        dateCreated timestamp
    );"""
CREATE_TABLE_Permission = """
    CREATE TABLE Permission (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        userId INTEGER NOT NULL,
        role TEXT,
        expiration timestamp NULL,
        dateCreated timestamp,
        CONSTRAINT fk_user
            FOREIGN KEY (userId)
            REFERENCES User(id)
            ON DELETE CASCADE
    );"""
CREATE_TABLE_PollingHistory = """
    CREATE TABLE PollingHistory (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        sent INTEGER,
        message TEXT,
        dateCreated timestamp
    );"""
CREATE_TABLE_SystemConfiguration = """
    CREATE TABLE SystemConfiguration (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        systemConfigurationTypeId INTEGER NOT NULL,
        value TEXT,
        dateCreated timestamp
    );"""
CREATE_TABLE_SystemConfigurationType = """
    CREATE TABLE SystemConfigurationType (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        enumCode TEXT,
        description TEXT,
        dateCreated timestamp
    );"""
CREATE_DATA_SystemConfigurationType = """
    INSERT INTO SystemConfigurationType (enumCode, description, dateCreated)
    VALUES
    ('polling_enabled', 'true or false', STRFTIME('%Y-%m-%dT%H:%M:%f', DATE('now'))),
    ('polling_interval', 'number in seconds', STRFTIME('%Y-%m-%dT%H:%M:%f', DATE('now')))
    """
CREATE_TABLE_NotificationType = """
    CREATE TABLE NotificationType (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        enumCode TEXT,
        dateCreated timestamp
    );"""
CREATE_DATA_NotificationTypes = """
    INSERT INTO NotificationType (enumCode, dateCreated)
    VALUES
    ('alarm', STRFTIME('%Y-%m-%dT%H:%M:%f', DATE('now'))),
    ('push', STRFTIME('%Y-%m-%dT%H:%M:%f', DATE('now'))),
    ('banner', STRFTIME('%Y-%m-%dT%H:%M:%f', DATE('now')))
    """
CREATE_TABLE_NotificationScheduleOverride = """
    CREATE TABLE NotificationScheduleOverride (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        notificationTypeId INTEGER NOT NULL,
        enabled INTEGER NOT NULL,
        startDate timestamp NOT NULL,
        stopDate timestamp,
        dateCreated timestamp
    );"""
DROP_TABLE_RequestHistory = """
    DROP TABLE RequestHistory
"""
DROP_TABLE_StateHistory = """
    DROP TABLE StateHistory
"""
DROP_TABLE_User = """
    DROP TABLE User
"""
DROP_TABLE_Permission = """
    DROP TABLE Permission
"""
DROP_TABLE_PollingHistory = """
    DROP TABLE PollingHistory
"""
DROP_TABLE_SystemConfiguration = """
    DROP TABLE SystemConfiguration
"""
DROP_TABLE_SystemConfigurationType = """
    DROP TABLE SystemConfigurationType
"""
DROP_TABLE_NotificationType = """
    DROP TABLE NotificationType
"""
DROP_TABLE_NotificationScheduleOverride = """
    DROP TABLE NotificationScheduleOverride
"""

ALTER_TABLE_PollingHistory_AddRetriesColumn = """
    ALTER TABLE PollingHistory ADD COLUMN retries INTEGER NULL
"""

ALTER_TABLE_PollingHistory_AddRssiColumnTx = """
    ALTER TABLE PollingHistory ADD COLUMN rssiTx INTEGER NULL
"""
ALTER_TABLE_PollingHistory_AddRssiColumnRx = """
    ALTER TABLE PollingHistory ADD COLUMN rssiRx INTEGER NULL
"""


################# SQL APP USAGE #####################
InsertRequestHistory = "INSERT INTO RequestHistory (request, sent, error, dateCreated) values(?, ?, ?, ?)"
InsertStateHistory = "INSERT INTO StateHistory (addr, state, dateCreated) values(?, ?, ?)"
InsertUser = "INSERT INTO User (username, dateCreated) values (?, ?)"
InsertPermission = "INSERT INTO Permission (userId, role, expiration, dateCreated) values (?, ?, ?, ?)"
UpdateUser = "UPDATE User SET hashedPassword=? WHERE username=?"
UpdatePermission = """
UPDATE Permission SET role=?, expiration=?
WHERE EXISTS (SELECT 1 FROM User WHERE User.id = Permission.userId and User.username=?)
"""
InsertPollingHistory = "INSERT INTO PollingHistory (sent, message, retries, rssiTx, rssiRx, dateCreated) values(?, ?, ?, ?, ?, ?)"
InsertSystemConfiguration = "INSERT INTO SystemConfiguration(systemConfigurationTypeId, value, dateCreated) values(?,?,?)"
UpdateSystemConfiguration = "UPDATE SystemConfiguration SET value = ? WHERE systemConfigurationTypeId = ?"
InsertNotificationScheduleOverride = "INSERT INTO NotificationScheduleOverride (notificationTypeId, enabled, startDate, stopDate, dateCreated) values (?,?,?,?,?)"

GetRequestHistory = "SELECT id, request, sent, error, dateCreated FROM RequestHistory"
GetStateHistory = "SELECT id, addr, state, dateCreated FROM StateHistory"
GetMostRecentState = "SELECT id, addr, state, dateCreated FROM StateHistory ORDER BY dateCreated DESC LIMIT 1"
GetUser = """SELECT User.id, username, hashedPassword, role, expiration, User.dateCreated FROM User
INNER JOIN Permission ON Permission.userId = User.id
WHERE User.username = ?
ORDER BY User.dateCreated DESC
"""
GetUsers = """SELECT User.id, username, hashedPassword, role, expiration, User.dateCreated FROM User
INNER JOIN Permission ON Permission.userId = User.id
ORDER BY User.dateCreated DESC
"""
GetPollingHistory = "SELECT id, sent, message, retries, rssiTx, rssiRx, dateCreated FROM PollingHistory"
GetSystemConfigurations = "SELECT id, systemConfigurationTypeId, value, dateCreated FROM SystemConfiguration"
GetSystemConfigurationByTypeId = "SELECT id, systemConfigurationTypeId, value, dateCreated FROM SystemConfiguration WHERE systemConfigurationTypeId = ?"
GetSystemConfigurationTypes = "SELECT id, enumCode, description FROM SystemConfigurationType"
GetSystemConfigurationTypeById = "SELECT id, enumCode, description FROM SystemConfigurationType WHERE id = ?"
GetSystemConfigurationTypeByEnumCode = "SELECT id, enumCode, description FROM SystemConfigurationType WHERE enumCode = ?"
GetNotificationTypeId = "SELECT id FROM NotificationType WHERE enumCode = ?"
GetNotificationScheduleOverrideByDate = "SELECT id, enabled, startDate, stopDate, dateCreated FROM NotificationScheduleOverride WHERE ?2 >= startDate AND (?2 <= stopDate OR stopDate IS NULL) AND notificationTypeId = ?1"

DeleteAllRequestHistory = "DELETE FROM RequestHistory"
DeleteAllStateHistory = "DELETE FROM StateHistory"
DeleteOldestRequestHistory = "DELETE FROM RequestHistory WHERE dateCreated < date('now','-7 day')"
DeleteOldestStateHistory = "DELETE FROM StateHistory WHERE dateCreated < date('now','-7 day')"
DeleteOldestPollingHistory = "DELETE FROM PollingHistory WHERE dateCreated < date('now','-8 day')"
DeleteSystemConfiguration = "DELETE FROM SystemConfiguration WHERE systemConfigurationTypeId = ?"
DeleteUser = "DELETE FROM User WHERE username = ?"
DeleteAllNotificationType = "DELETE FROM NotificationType"
DeleteNotificationScheduleOverride = "DELETE FROM NotificationScheduleOverride WHERE id = ?"
DeleteAllNotificationScheduleOverride = "DELETE FROM NotificationScheduleOverride"

class Repository:
    def __init__(self):
        connString = dbConnString()
        self.dbConn = sqlite3.connect(**connString)
        
        # Migration if necessary
        current_db_version = self.getDatabaseVersion() # Can be 0
        self.runMigration(current_db_version, DATABASE_VERSION)

        self.dbConn.execute("PRAGMA foreign_keys = ON;") # Turn on foreign keys

        connSuccess = self.checkConnection()
        logger.info("[Database] Connecting to " + str(connString))
        logger.info("[Database] Connected: " + str(connSuccess))

    ### Migrates up or down fromVersion toVersion
    def runMigration(self, fromVersion, toVersion):
        self.dbConn.execute("PRAGMA foreign_keys = OFF;") # Turn off foreign keys
          
        # Migrate up
        if (fromVersion < toVersion):
            logger.info("[Database] Beginning Migration from v" + str(fromVersion) + " to v" + str(toVersion))
            while (fromVersion < toVersion):
                migratedTo = self.migrateUp(fromVersion)
                fromVersion = migratedTo
                self.setDatabaseVersion(migratedTo)

        # Migrate down
        elif (fromVersion > toVersion):
            logger.info("[Database] Beginning Downgrade from v" + str(fromVersion) + " to v" + str(toVersion))
            while (fromVersion > toVersion):
                migratedTo = self.migrateDown(fromVersion)
                fromVersion = migratedTo
                self.setDatabaseVersion(migratedTo)

    def migrateUp(self, fromVersion):
        logger.info("[Database] Upgrading from v" + str(fromVersion) + " to v" + str(fromVersion + 1))
        with self.dbConn:
            # Initial database
            if (fromVersion == 0): # To v1
                # Hack because db version was added after the first release and may exist as version None
                if dbFirstRun():
                    self.dbConn.execute(CREATE_TABLE_RequestHistory)
                    self.dbConn.execute(CREATE_TABLE_StateHistory)
                    self.dbConn.execute(CREATE_TABLE_User)
                    self.dbConn.execute(CREATE_TABLE_Permission)
                    logger.info("[Database] Created Sqlite3 Database at v1")
            # Adds PollingHistory table
            if (fromVersion == 1): # To v2
                self.dbConn.execute(CREATE_TABLE_PollingHistory)
            # Add retries column to PollingHistory table
            if (fromVersion == 2): # To v3
                self.dbConn.execute(ALTER_TABLE_PollingHistory_AddRetriesColumn)
            # Add NotificationType, NotificationScheduleOverride tables, crud ops
            if (fromVersion == 3): # To v4
                self.dbConn.execute(CREATE_TABLE_NotificationType)
                self.dbConn.execute(CREATE_TABLE_NotificationScheduleOverride)
                self.dbConn.execute(CREATE_DATA_NotificationTypes)
            # Add rssi tx column, rssi rx column
            if (fromVersion == 4): # To v5
                self.dbConn.execute(ALTER_TABLE_PollingHistory_AddRssiColumnTx)
                self.dbConn.execute(ALTER_TABLE_PollingHistory_AddRssiColumnRx)
            if (fromVersion == 5): # To v6
                self.dbConn.execute(CREATE_TABLE_SystemConfiguration)
                self.dbConn.execute(CREATE_TABLE_SystemConfigurationType)
                self.dbConn.execute(CREATE_DATA_SystemConfigurationType)
            
        return fromVersion + 1

    def migrateDown(self, fromVersion):
        with self.dbConn:
            if (fromVersion == 6):
                self.dbConn.execute(DROP_TABLE_SystemConfiguration)
                self.dbConn.execute(DROP_TABLE_SystemConfigurationType)
            # Remove NotificationType and NotificationScheduleOverride tables, and seed data
            if (fromVersion == 4):
                self.dbConn.execute(DROP_TABLE_NotificationType)
                self.dbConn.execute(DROP_TABLE_NotificationScheduleOverride)
                self.dbConn.execute(DeleteAllNotificationType)
            # Remove Retries, rssi columns from PollingHistory table
            if (fromVersion == 3 or fromVersion == 5):
                #sqlite does not support drop column, instead drop and re-create the table without it
                self.dbConn.execute(DROP_TABLE_PollingHistory)
                self.dbConn.execute(CREATE_TABLE_PollingHistory)
            # Remove PollingHistory table
            if (fromVersion == 2):
                self.dbConn.execute(DROP_TABLE_PollingHistory)
            # Delete it all
            if (fromVersion == 1):
                self.dbConn.execute(DROP_TABLE_RequestHistory)
                self.dbConn.execute(DROP_TABLE_StateHistory)
                self.dbConn.execute(DROP_TABLE_User)
                self.dbConn.execute(DROP_TABLE_Permission)
                logger.info("[Database] Database Deleted!")

        return fromVersion - 1

    def checkConnection(self):
     try:
        self.dbConn.cursor()
        return True
     except Exception as ex:
        return False

    ################# Metadata #####################
    def getDatabaseVersion(self):
        cur = self.dbConn.cursor()
        cur.execute(GET_DATABASE_VERSION)
        return (cur.fetchone() or [0])[0]
    
    def setDatabaseVersion(self, version):
        self.dbConn.execute(SET_DATABASE_VERSION.format(v=version))
        self.dbConn.commit()

    ################# Data access layer #####################
    def insertRequestHistory(self, request, result):
        cur = self.dbConn.cursor()
        cur.execute(InsertRequestHistory, [request, result.success, result.message, datetime.now().isoformat()])
        self.dbConn.commit()
        return cur.lastrowid

    def insertStateHistory(self, addr, state):
        cur = self.dbConn.cursor()
        cur.execute(InsertStateHistory, [addr, state, datetime.now().isoformat()])
        self.dbConn.commit()
        return cur.lastrowid

    def insertUser(self, username, role, expiration=None):
        cur = self.dbConn.cursor()
        cur.execute(InsertUser, [username, datetime.now().isoformat()])
        userId = cur.lastrowid
        cur.execute(InsertPermission, [userId, role, expiration, datetime.now().isoformat()])
        self.dbConn.commit()
        return userId

    def insertPollingtHistory(self, result, rssiTx, rssiRx):
        cur = self.dbConn.cursor()
        cur.execute(InsertPollingHistory, [result.success, result.message, result.retries, rssiTx, rssiRx, datetime.now().isoformat()])
        cur.execute(DeleteOldestPollingHistory)
        self.dbConn.commit()
        return cur.lastrowid
    
    def SaveSystemConfiguration(self, systemConfigurationType, value):
        cur = self.dbConn.cursor()

        cur.execute(GetSystemConfigurationTypeByEnumCode, [systemConfigurationType])
        systemConfigurationTypeId = cur.fetchone()[0]

        if (systemConfigurationTypeId is None):
            return False, 'SystemConfigurationTypeId not found'

        cur.execute(GetSystemConfigurationByTypeId, [systemConfigurationTypeId])
        existingEntity = cur.fetchone()
        
        # Upsert
        if (existingEntity is not None):
            cur.execute(UpdateSystemConfiguration, [value, systemConfigurationTypeId])
        else:
            cur.execute(InsertSystemConfiguration, [systemConfigurationTypeId, value, datetime.now().isoformat()])
        
        self.dbConn.commit()
        return True, 'Success'

    def insertNotificationScheduleOverride(self, notificationType, enabled, startDate, stopDate=None):
        cur = self.dbConn.cursor()
        cur.execute(GetNotificationTypeId, [notificationType])
        notificationTypeId = cur.fetchone()[0]
        if (notificationTypeId is not None):
            cur.execute(InsertNotificationScheduleOverride, [notificationTypeId, enabled, startDate, stopDate, datetime.now().isoformat()])
            self.dbConn.commit()
            return True
        return False

    def updateUser(self, username, role, expiration=None):
        cur = self.dbConn.cursor()
        cur.execute(UpdatePermission, [role, expiration, username])
        self.dbConn.commit()

    def updateUserPassword(self, username, hasedPassword):
        cur = self.dbConn.cursor()
        cur.execute(UpdateUser, [hasedPassword, username])
        self.dbConn.commit()

    def getRequestHistory(self):
        cur = self.dbConn.cursor()
        cur.execute(GetRequestHistory)
        return cur.fetchall()

    def getStateHistory(self):
        cur = self.dbConn.cursor()
        cur.execute(GetStateHistory)
        return cur.fetchall()

    def getMostRecentState(self):
        cur = self.dbConn.cursor()
        cur.execute(GetMostRecentState)
        return cur.fetchone()

    def getUser(self, username):
        cur = self.dbConn.cursor()
        cur.execute(GetUser, [username.lower()])
        record = cur.fetchone()
        return None if record is None else User(record[0], record[1], record[2], record[3], record[4], record[5])
        
    def getUsers(self):
        cur = self.dbConn.cursor()
        cur.execute(GetUsers)
        records = cur.fetchall()
        if records is None:
            return []
        return [User(record[0], record[1], record[2], record[3], record[4], record[5]) for record in records]
        
    def getPollingHistory(self):
        cur = self.dbConn.cursor()
        cur.execute(GetPollingHistory)
        return cur.fetchall()
    
    def getSystemConfigurations(self):
        cur = self.dbConn.cursor()
        cur.execute(GetSystemConfigurations)
        
        records = cur.fetchall()
        if records is None:
            return []
        
        # Join with the reference table
        for record in records:
            cur.execute(GetSystemConfigurationTypeById, [record[1]])
            systemConfigurationType = cur.fetchone()
            yield SystemConfiguration(record[0], systemConfigurationType[1], systemConfigurationType[2], record[2], record[3])
    
    def getSystemConfiguration(self, systemConfigurationTypeEnumCode):
        cur = self.dbConn.cursor()
        cur.execute(GetSystemConfigurationTypeByEnumCode, [systemConfigurationTypeEnumCode])

        systemConfigurationType = cur.fetchone()

        if (systemConfigurationType is None):
            return None
        
        cur.execute(GetSystemConfigurationByTypeId, [systemConfigurationType[0]])

        record = cur.fetchone()
        if (record is None):
            return None

        return SystemConfiguration(record[0], systemConfigurationType[1], systemConfigurationType[2], record[2], record[3])

    def getSystemConfigurationTypes(self):
        cur = self.dbConn.cursor()
        cur.execute(GetSystemConfigurationTypes)
        return cur.fetchall()

    def getNotificationScheduleOverrideByDate(self, notificationType, when):
        cur = self.dbConn.cursor()
        cur.execute(GetNotificationTypeId, [notificationType])
        notificationTypeId = cur.fetchone()[0]
        if (notificationTypeId is not None):
            cur.execute(GetNotificationScheduleOverrideByDate, [notificationTypeId, when])
            return cur.fetchall()
        return None

    def deleteAllHistory(self):
        cur = self.dbConn.cursor()
        cur.execute(DeleteAllRequestHistory)
        cur.execute(DeleteAllStateHistory)
        self.dbConn.commit()

    def deleteOldHistory(self):
        cur = self.dbConn.cursor()
        cur.execute(DeleteOldestRequestHistory)
        cur.execute(DeleteOldestStateHistory)
        self.dbConn.commit()
    
    def deleteUser(self, username):
        cur = self.dbConn.cursor()
        cur.execute(DeleteUser, [username])
        self.dbConn.commit()

    def deleteSystemConfiguration(self, systemConfigurationType):
        cur = self.dbConn.cursor()

        cur.execute(GetSystemConfigurationTypeId, [systemConfigurationType])

        systemConfigurationTypeId = cur.fetchone()[0]

        if (not systemConfigurationTypeId):
            return False, 'Invalid system configuration type'

        cur.execute(DeleteSystemConfiguration, [systemConfigurationTypeId])
        self.dbConn.commit()
        return True, 'Success'

    def deleteAllNotificationScheduleOverride(self):
        cur = self.dbConn.cursor()
        cur.execute(DeleteAllNotificationScheduleOverride)
        self.dbConn.commit()

    def deleteNotificationScheduleOverride(self, id):
        cur = self.dbConn.cursor()
        cur.execute(DeleteNotificationScheduleOverride, [id])
        self.dbConn.commit()


repository = Repository()