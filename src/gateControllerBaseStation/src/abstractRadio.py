import threading
from fileLogger import logger

class AbstractRadio:
    def __init__(self):
        self.listeners = []

    def addMessageListener(self, listener):
        logger.info("[Radio] Added listener " + str(listener))
        self.listeners.append(listener)

    def informListeners(self, address, currentState):
        for listener in self.listeners:
            listener(address, currentState)

    '''
    Listener method for worker
    '''
    def listen(self):
        pass

    '''
    Non-Blocking message callback
    '''
    def receiveMessage(self, messaeg):
        pass

    '''
    Send message (broadcast wait for ack)
    '''
    def sendBroadcast(self, command, stop=True):
        pass
    
    '''
    Send message to remote (wait for response)
    '''
    def sendToRemote(self, command, stop=True):
        pass

    '''
    Run a local (non-transmit) command on the radio
    '''
    def runLocalCommand(self, command, type='AT'):
        pass
    
    '''
    Run a remote command on the radio
    '''
    def runRemoteCommand(self, command, type='AT'):
        pass
    
    