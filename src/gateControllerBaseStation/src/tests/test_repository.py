# import unittest
import pytest
from repository import repository
from datetime import datetime  
from datetime import timedelta


@pytest.fixture(autouse=True)
def run_before_and_after_tests(tmpdir):
    """Fixture to execute asserts before and after a test is run"""
    # Setup: fill with any logic you want

    yield # this is where the testing happens

    # Teardown : fill with any logic you want
    repository.deleteAllNotificationScheduleOverride()


class TestNotificationScheduleOverride:
    
    def test_notificationSchedule_Expiration(self):
        now = datetime.now()
        sec60 = timedelta(seconds=60)
        sec61 = timedelta(seconds=61)

        # No data should exist
        notificationSchedule = repository.getNotificationScheduleOverrideByDate('alarm', now)
        assert notificationSchedule == []

        # notification status inserted, 2 min window where 'now' in the middle
        insertResult = repository.insertNotificationScheduleOverride('alarm', 1, now - sec60, now + sec60)
        assert insertResult == True

        # One record should exist
        notificationSchedule2 = repository.getNotificationScheduleOverrideByDate('alarm', now)
        assert len(notificationSchedule2) == 1

        # No records exist after expiration window
        notificationSchedule3 = repository.getNotificationScheduleOverrideByDate('alarm', now + sec61)
        assert notificationSchedule3 == []
    
    def test_notificationSchedule_noStop(self):
        now = datetime.now()
        sec60 = timedelta(seconds=60)

        # notification status inserted, 2 min window where 'now' in the middle
        insertResult = repository.insertNotificationScheduleOverride('alarm', 1, now - sec60) # Stop date not provided, defaults to no stop date
        assert insertResult == True

        # One record should exist
        notificationSchedule2 = repository.getNotificationScheduleOverrideByDate('alarm', now)
        assert len(notificationSchedule2) == 1

        # Record still exists
        notificationSchedule3 = repository.getNotificationScheduleOverrideByDate('alarm', now + sec60 + sec60)
        assert len(notificationSchedule3) == 1

    def test_notificationSchedule_interior_overlap(self):
        now = datetime.now()
        sec60 = timedelta(seconds=60)
        sec30 = timedelta(seconds=30)
        sec1 = timedelta(seconds=1)

        # [a[b]a] b schedule inside a schedule
        insertResult = repository.insertNotificationScheduleOverride('alarm', 1, now - sec60, now + sec60)
        assert insertResult == True

        insertResult = repository.insertNotificationScheduleOverride('alarm', 1, now - sec30, now + sec30)
        assert insertResult == True

        # Before overlap, one record should exist
        notificationSchedule1 = repository.getNotificationScheduleOverrideByDate('alarm', now - sec30 - sec1)
        assert len(notificationSchedule1) == 1

        # During overlap, two records should exist
        notificationSchedule2 = repository.getNotificationScheduleOverrideByDate('alarm', now)
        assert len(notificationSchedule2) == 2

        # After overlap, one record should exist
        notificationSchedule3 = repository.getNotificationScheduleOverrideByDate('alarm', now + sec30 + sec1)
        assert len(notificationSchedule3) == 1

        
        # After expiration, no records exist
        notificationSchedule4 = repository.getNotificationScheduleOverrideByDate('alarm', now + sec60 + sec1)
        assert len(notificationSchedule4) == 0

    def test_notificationSchedule_exterior_overlap(self):
        now = datetime.now()
        sec60 = timedelta(seconds=60)
        sec30 = timedelta(seconds=30)
        sec1 = timedelta(seconds=1)

        # [a[ba]b] the end of schedule a overlaps with the start of schedule b
        insertResult = repository.insertNotificationScheduleOverride('alarm', 1, now - sec30, now + sec30)
        assert insertResult == True

        insertResult = repository.insertNotificationScheduleOverride('alarm', 1, now + sec1, now + sec60)
        assert insertResult == True

        # Before overlap, one record should exist
        notificationSchedule1 = repository.getNotificationScheduleOverrideByDate('alarm', now)
        assert len(notificationSchedule1) == 1

        # During overlap, two records should exist
        notificationSchedule2 = repository.getNotificationScheduleOverrideByDate('alarm', now + sec1)
        assert len(notificationSchedule2) == 2

        # After overlap, one record should exist
        notificationSchedule3 = repository.getNotificationScheduleOverrideByDate('alarm', now + sec30 + sec1)
        assert len(notificationSchedule3) == 1

        
        # After expiration, no records exist
        notificationSchedule4 = repository.getNotificationScheduleOverrideByDate('alarm', now + sec60 + sec1)
        assert len(notificationSchedule4) == 0

    def test_notificationSchedule_boundaries(self):
        now = datetime.now()
        sec30 = timedelta(seconds=30)

        # Inclusive boundaries
        insertResult = repository.insertNotificationScheduleOverride('alarm', 1, now - sec30, now + sec30)
        assert insertResult == True

        # On start time
        notificationSchedule1 = repository.getNotificationScheduleOverrideByDate('alarm', now - sec30)
        assert len(notificationSchedule1) == 1

        # Inside schedule
        notificationSchedule2 = repository.getNotificationScheduleOverrideByDate('alarm', now)
        assert len(notificationSchedule2) == 1

        # On stop time
        notificationSchedule3 = repository.getNotificationScheduleOverrideByDate('alarm', now + sec30)
        assert len(notificationSchedule3) == 1

        
    def test_notificationSchedule_delete(self):
        now = datetime.now()
        sec30 = timedelta(seconds=30)

        insertResult = repository.insertNotificationScheduleOverride('alarm', 1, now - sec30, now + sec30)
        assert insertResult == True

        # Inside schedule
        notificationSchedule1 = repository.getNotificationScheduleOverrideByDate('alarm', now)
        assert len(notificationSchedule1) == 1

        # Delete schedule by id
        repository.deleteNotificationScheduleOverride(notificationSchedule1[0][0])

        # Schedule no longer exists
        notificationSchedule2 = repository.getNotificationScheduleOverrideByDate('alarm', now)
        assert len(notificationSchedule2) == 0

