class ProgramKilled(Exception):
    pass

def signal_handler(signum, frame):
    raise ProgramKilled