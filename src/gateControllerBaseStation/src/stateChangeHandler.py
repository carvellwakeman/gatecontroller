from datetime import datetime, timedelta
import os
from random import randrange
from flask_socketio import emit
from helpers import isCloseToOpen, isOpenToClose
from audio import audio
from repository import repository
from fileLogger import logger
from environment import soundNotifyWindowStart, soundNotifyWindowEnd
from helpers import byteStateToString

class StateChangeHandler:
    def __init__(self):
        pass

    def setSocketio(self, socketio):
        self.socketio = socketio

    def handleStateChange(self, address, currentStateEnum):        
        # Socket event
        currentState = byteStateToString(currentStateEnum)
        try:
            self.socketio.emit("stateChange", currentState)
            logger.info("[SocketIO] Broadcast State " + currentState)
        except Exception as ex:
            logger.error("[SocketIO] Error broadcasting state " + str(ex))
        # Get historical state
        recentState = repository.getMostRecentState()

        now = datetime.now()

        # Persist latest state if changed
        if (recentState == None):
            logger.info("[StateChangeHandler] at " + str(now) + " First state is " + currentStateEnum)
            repository.insertStateHistory(address, currentStateEnum)
        elif (currentStateEnum != recentState[2] and currentStateEnum != 'u'):
            logger.info("[StateChangeHandler] at " + str(now) + " State is now " + currentStateEnum + " (was: " + recentState[2] + ")")
            repository.insertStateHistory(address, currentStateEnum)
            
            # Notify (alarm)
            if now.hour >= soundNotifyWindowStart or now.hour <= soundNotifyWindowEnd:
                alarmSchedules = repository.getNotificationScheduleOverrideByDate('alarm', now)
                if len(alarmSchedules) == 0 or alarmSchedules[0][2] == 1: # no schedule or explicitly enabled
                    n = randrange(11) + 1
                    # Closing
                    if (isOpenToClose(recentState[2], currentStateEnum)):
                        audio.play('./sound/bells/single/shipbell'+str(n)+'.wav')
                    # Opening
                    elif (isCloseToOpen(recentState[2], currentStateEnum)):
                        audio.play('./sound/bells/double/shipbell'+str(n)+'.wav')
            
            # Delete old history
            repository.deleteOldHistory()
        else:
            logger.info("[StateChangeHandler] at " + str(now) + " State Unchanged (is: " + recentState[2] + ")")

stateChangeHandler = StateChangeHandler()