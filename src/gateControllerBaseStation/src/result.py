class Result:
  def __init__(self, success, addr, message, retries = 0):
    self.success = success # bool
    self.addr = addr # string
    self.retries = retries
    self.message = message # string (error when success = false)

  def toJson(self):
    return { 'success': self.success, 'addr': self.addr, 'message': self.message, 'retries': self.retries }
