# from pygame import mixer
from pydub import AudioSegment
from pydub.playback import play
from fileLogger import logger
from pathlib import Path

class Audio:
    def __init__(self):
        pass
        # mixer.init(48000, -16, 1, 1024)
        # mixer.music.set_volume(0.0)

    def play(self, file):
        try:
            combinedPath = Path.cwd().joinpath(file).resolve()

            sound = AudioSegment.from_file(str(combinedPath), format="wav")
            play(sound)
            logger.info("[Audio] Played file " + str(combinedPath))

            # mixer.music.set_volume(1.0)
            # mixer.music.load(file)
            # mixer.music.play()
            # while (mixer.music.get_busy()):
            #     continue
            # mixer.music.set_volume(0.0)
        except Exception as e:
            logger.error("[Audio] File " + file + " not played: " + str(e))
            pass

audio = Audio()
