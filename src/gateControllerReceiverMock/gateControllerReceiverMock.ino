
/******************************************************************************
* Gate Controller Slave Mocked (moves gate) - Zach
*******************************************************************************/

#include "Arduino.h"
#include <SPI.h>
#include <SoftwareSerial.h>

#define NUM_INSTRUCTIONS 32
#define DATA_WAIT_THRESHOLD 500
#define NEXT_PACKET_WAIT_THRESHOLD 5000


// Radio
#define PIN_DOUT 11
#define PIN_DIN 12
SoftwareSerial nss(PIN_DOUT, PIN_DIN);
uint8_t* data;
uint8_t dataLength;
String strBuff;


char ackMsg = 'u'; // Defaults to unknown
char lastAckMsg = 'u'; // Defaults to unknown

// Receiving
unsigned long now = 0;
unsigned long lastReceive = 0;
unsigned long lastInstructionTime = 0;
unsigned long lastInstructionStop = 0;
unsigned long dataWaitThreshold = DATA_WAIT_THRESHOLD;

// States
unsigned long lastSwitchTime = 0;
long debounce = 200;   // the debounce time, increase if the output flickers
int gateInstructions[NUM_INSTRUCTIONS] = {};
int gateInstructionsStart = 0;
int gateInstructionsEnd = 0;
unsigned int instruction = 0;
unsigned int myWait = 0;

void setup()
{
    // Logging
    Serial.begin(9600);

    // Radio
    nss.begin(9600);

    Serial.println();
    Serial.println("*** Finished Boot (Slave) [MOCKED]");
}

void loop()
{
    now = millis();

    if (millis() - lastSwitchTime > debounce) {
        lastSwitchTime = now;

        if (lastAckMsg != ackMsg) {
            lastAckMsg = ackMsg;
        }
    }
    
    // Wait at least half a second since the last command before running anything
    if (now - lastReceive > dataWaitThreshold) {
        // Reset wait threshold in case it was changed
        dataWaitThreshold = DATA_WAIT_THRESHOLD;

        // Wait if last instruction was a wait command
        if ((now - lastInstructionTime) >= myWait*1000) {
            // Next instruction
            instruction = getInstruction();

            if (instruction != 0) {
                lastInstructionTime = millis();

                Serial.print("RUN CMD "); Serial.println(instruction);

                // Wait time (minimum wait of PIN_WRITE_DELAY between instructions, maximum of Instruction time)
                if (instruction > 5)
                {
                    myWait = instruction;
                }
                else {
                    myWait = 0;

                    // Open gate
                    if (instruction == 1)
                    {
                        openGate();
                    }
                    // Stop gate
                    else if (instruction == 2)
                    {
                        stopGate();
                    }
                    // Close gate
                    else if (instruction == 3)
                    {
                        closeGate();
                    }
                    // Gate state
                    else if (instruction == 4) {
                        // Do nothing, just ack with state
                        sendState(ackMsg);
                    }
                    // Mocks only
                    else if (instruction == 5) {
                        sendState('m');
                    }
                }
            }
        }
        //else if (lastInstructionTime != 0) { Serial.println("Executing Wait Instruction"); }
    }
    //else if (lastReceive != 0) { Serial.println("Waiting for more data"); }
    
    // Receive radio data
    receiveCommand();
}


//==============================================================================
// Debugging
//==============================================================================
void printStatus()
{
    for (int i = 0; i < NUM_INSTRUCTIONS; i++)
    {
        Serial.print(" [");
        Serial.print(gateInstructions[i]);
        if (gateInstructionsStart == i)
        {
            Serial.print("s");
        }
        if (gateInstructionsEnd == i)
        {
            Serial.print("e");
        }
        Serial.print("] ");
    }

    if (gateInstructionsEnd == NUM_INSTRUCTIONS)
    {
        Serial.print(" e");
    }

    Serial.println();
}


//==============================================================================
// Instructions
// 0 = do nothing, 1 = open gate, 2 = stop gate, 3 = close gate, 4 = get state, >3 = wait seconds
//==============================================================================
bool addInstruction(int instruction)
{
    // Cannot over-fill array
    if (gateInstructionsStart == gateInstructionsEnd && gateInstructions[gateInstructionsEnd] != 0)
    {
        Serial.println("Ignoring instruction because instructions array is full.");
        return false;
    }

    gateInstructions[gateInstructionsEnd] = instruction;
    gateInstructionsEnd++;

    // Wrap around end
    if (gateInstructionsEnd >= NUM_INSTRUCTIONS)
    {
        gateInstructionsEnd = 0;
    }

    return true;
}

int getInstruction()
{
    int oldestInstruction = gateInstructions[gateInstructionsStart];
    if (oldestInstruction > 0)
    {
        gateInstructions[gateInstructionsStart] = 0;
        gateInstructionsStart++;
    }

    // Pop an item, move the start up
    if (gateInstructionsStart >= NUM_INSTRUCTIONS)
    {
        gateInstructionsStart = 0;
    }

    return oldestInstruction;
}


//==============================================================================
// Hardware actions
//==============================================================================
void openGate()
{
    ackMsg = 'o';
    Serial.println("Gate mocked open");
}

void stopGate()
{
    ackMsg = 'm';
    Serial.println("Gate mocked stop");
}

void closeGate()
{
    ackMsg = 'c';
    Serial.println("Gate mocked close");
}


//==============================================================================
// Xbee methods
//==============================================================================
void receiveCommand() {
    // Read packet and store in buffer
    if (Serial.available() > 0)
    {
        strBuff = Serial.readStringUntil('\n');

        Serial.println("the sender got an ACK");

        Serial.println(strBuff);
        Serial.flush();
    }
    else
    {
        return;
    }

    // Parse buffer
    int commaIndex = strBuff.indexOf(',');
    String strCommand = strBuff.substring(0, commaIndex);
    String strStop = strBuff.substring(commaIndex + 1);

    int command = strCommand.toInt();
    bool stop = (bool)strStop.toInt();

    // Clear buffer
    data = 0;

    // Process command
    if (command > 0) {
        Serial.print("RECV "); Serial.print(command);
        Serial.print(" ACK STOP="); Serial.println(stop);
        addInstruction(command);
        lastReceive = millis();
        
        if (stop == false) {
            dataWaitThreshold = NEXT_PACKET_WAIT_THRESHOLD;
            Serial.print("EXPECT STOP WITHIN "); Serial.print(NEXT_PACKET_WAIT_THRESHOLD); Serial.println(" MS");
        }
    }
}

void sendState(char state) {
    // Transmit to serial
    // XBeeAddress64 addr64 = XBeeAddress64(0x0013A200, 0x41C429AF);
    // ZBTxRequest zbTx = ZBTxRequest(addr64, state, sizeof(state));
    
    // Broadcast
    // ZBTxRequest zbTx = ZBTxRequest();
    // zbTx.setPayload(state);
    // zbTx.setPayloadLength(sizeof(state));

    // Hacky thing because sending a single char always sends 00
    uint8_t payload[1];
    payload[0] = state;

    Serial.print("FINISH SEND "); Serial.println(state);
}