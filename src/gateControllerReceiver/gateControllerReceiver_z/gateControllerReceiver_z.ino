
/******************************************************************************
* Gate Controller Receiver (moves gate) - Arduino Nano && XBee series 3
*******************************************************************************/

#include "Arduino.h"
#include <SPI.h>
#include <XBee.h>
#include <SoftwareSerial.h>

// Debug
//#define DEBUG

// MC
#define MC_BAUD 9600

// Pins
#define PIN_OFF HIGH
#define PIN_ON LOW
#define SENSOR_ON HIGH
#define SENSOR_OFF LOW
#define SENSOR_PINMODE INPUT // Input_pullup when pin won't stay stable
#define PIN_SWITCH_OPEN 2  // D2 is Gate Open limit switch
#define PIN_SWITCH_CLOSE 3 // D3 is Gate Closed limit switch
#define PIN_OPEN_GATE 5    // D5 is Open Gate relay
#define PIN_STOP_GATE 8    // D8 is Stop Gate relay
#define PIN_CLOSE_GATE 9   // D9 is Close Gate relay

#define PIN_WRITE_DELAY 1      // How long to wait after activating a relay
#define PIN_WRITE_DURATION 500 // How long to hold relay closed

#define NUM_INSTRUCTIONS 32
#define DATA_WAIT_THRESHOLD 500
#define NEXT_PACKET_WAIT_THRESHOLD 1000

// Radio
#define PIN_DOUT 11
#define PIN_DIN 12
#define ReceiveCommand receiveCommand_series3

// Sensor state to gate state
bool isGateOpen(int sensor1, int sensor2) {
    return sensor1 == SENSOR_ON && sensor2  == SENSOR_OFF;
}
bool isGateMoving(int sensor1, int sensor2) {
    return sensor1 == SENSOR_OFF && sensor2 == SENSOR_OFF;
}
bool isGateClosed(int sensor1, int sensor2) {
    return sensor1 == SENSOR_OFF && sensor2 == SENSOR_ON;
}


// ****************************************** //
SoftwareSerial nss(PIN_DOUT, PIN_DIN);
XBee xbee = XBee();
XBeeResponse response = XBeeResponse();
ZBRxResponse rx = ZBRxResponse();
char *data;
String strBuff;

char ackMsg = 'u'; // Defaults to unknown

// Receiving
// char recvBuff[128]; // 128 should be enough for 7 days in seconds + , + 1/0 stop bit
unsigned long now = 0;
unsigned long lastReceive = 0;
unsigned long lastInstructionTime = 0;
unsigned long lastInstructionStop = 0;
unsigned long dataWaitThreshold = DATA_WAIT_THRESHOLD;

// States
int btnGateOpenState = 0;
int btnGateOpenStatePrev = 0;
int btnGateClosedState = 0;
int btnGateClosedStatePrev = 0;
unsigned long lastSwitchTime = 0;
long debounce = 200; // the debounce time, increase if the output flickers
int gateInstructions[NUM_INSTRUCTIONS] = {};
int gateInstructionsStart = 0;
int gateInstructionsEnd = 0;
unsigned int instruction = 0;
unsigned int myWait = 0;

void setup()
{
    // Logging
    Serial.begin(MC_BAUD);
    pinMode(PIN_OPEN_GATE, OUTPUT);
    pinMode(PIN_STOP_GATE, OUTPUT);
    pinMode(PIN_CLOSE_GATE, OUTPUT);
    pinMode(PIN_SWITCH_OPEN, SENSOR_PINMODE);
    pinMode(PIN_SWITCH_CLOSE, SENSOR_PINMODE);

    // Reset to off state
    digitalWrite(PIN_OPEN_GATE, PIN_OFF);
    digitalWrite(PIN_STOP_GATE, PIN_OFF);
    digitalWrite(PIN_CLOSE_GATE, PIN_OFF);

    // Radio
    nss.begin(9600);
    xbee.setSerial(nss);

    Serial.println();
    Serial.println("*** Finished Boot (Slave)");
}

void loop()
{
    now = millis();

    // Read limit switches
    btnGateOpenState = digitalRead(PIN_SWITCH_OPEN);
    btnGateClosedState = digitalRead(PIN_SWITCH_CLOSE);

    if (millis() - lastSwitchTime > debounce)
    {
        lastSwitchTime = now;

        if (btnGateOpenStatePrev != btnGateOpenState)
        {
            btnGateOpenStatePrev = btnGateOpenState;
        }
        if (btnGateClosedStatePrev != btnGateClosedState)
        {
            btnGateClosedStatePrev = btnGateClosedState;
        }

        char lastAckMsg = ackMsg;
        ackMsg = isGateMoving(btnGateOpenStatePrev, btnGateClosedStatePrev)
            ? 'm'
            : (isGateOpen(btnGateOpenStatePrev, btnGateClosedStatePrev)
                ? 'o'
                : (isGateClosed(btnGateOpenStatePrev, btnGateClosedStatePrev)
                    ? 'c'
                    : 'u'));

#ifdef DEBUG
        if (Serial.available())
        {
            char s = Serial.read();
            if (s == 'm' || s == 'o' || s == 'c' || s == 'u')
            {
                ackMsg = s;
            }
        }
#endif

        if (lastAckMsg != ackMsg)
        {
            sendState(ackMsg);
        }
    }

    // Wait at least half a second since the last command before running anything
    if (now - lastReceive > dataWaitThreshold)
    {
        // Reset wait threshold in case it was changed
        dataWaitThreshold = DATA_WAIT_THRESHOLD;

        // Wait if last instruction was a wait command
        if ((now - lastInstructionTime) >= myWait * 1000)
        {
            // Next instruction
            instruction = getInstruction();

            if (instruction != 0)
            {
                lastInstructionTime = millis();

                #ifdef DEBUG
                Serial.print("RUN CMD ");
                Serial.println(instruction);
                #endif

                // Wait time (minimum wait of PIN_WRITE_DELAY between instructions, maximum of Instruction time)
                if (instruction > 4)
                {
                    myWait = instruction;
                }
                else
                {
                    myWait = PIN_WRITE_DELAY;

                    // Open gate
                    if (instruction == 1)
                    {
                        openGate();
                    }
                    // Stop gate
                    else if (instruction == 2)
                    {
                        stopGate();
                    }
                    // Close gate
                    else if (instruction == 3)
                    {
                        closeGate();
                    }
                    // Gate state
                    else if (instruction == 4)
                    {
                        // Do nothing, just ack with state
                        sendState(ackMsg);
                    }
                }
            }
        }
        //else if (lastInstructionTime != 0) { Serial.println("Executing Wait Instruction"); }
    }
    //else if (lastReceive != 0) { Serial.println("Waiting for more data"); }

    // Receive radio data
    ReceiveCommand();

    //printStatus()
}

//==============================================================================
// Debugging
//==============================================================================
void printStatus()
{
    for (int i = 0; i < NUM_INSTRUCTIONS; i++)
    {
        Serial.print(" [");
        Serial.print(gateInstructions[i]);
        if (gateInstructionsStart == i)
        {
            Serial.print("s");
        }
        if (gateInstructionsEnd == i)
        {
            Serial.print("e");
        }
        Serial.print("] ");
    }

    if (gateInstructionsEnd == NUM_INSTRUCTIONS)
    {
        Serial.print(" e");
    }

    Serial.println();
}

//==============================================================================
// Instructions
// 0 = do nothing, 1 = open gate, 2 = stop gate, 3 = close gate, 4 = get state, >3 = wait milliseconds
//==============================================================================
bool addInstruction(int instruction)
{
    // Cannot over-fill array
    if (gateInstructionsStart == gateInstructionsEnd && gateInstructions[gateInstructionsEnd] != 0)
    {
        #ifdef DEBUG
        Serial.println("Ignoring instruction because instructions array is full.");
        #endif
        return false;
    }

    gateInstructions[gateInstructionsEnd] = instruction;
    gateInstructionsEnd++;

    // Wrap around end
    if (gateInstructionsEnd >= NUM_INSTRUCTIONS)
    {
        gateInstructionsEnd = 0;
    }

    return true;
}

int getInstruction()
{
    int oldestInstruction = gateInstructions[gateInstructionsStart];
    if (oldestInstruction > 0)
    {
        gateInstructions[gateInstructionsStart] = 0;
        gateInstructionsStart++;
    }

    // Pop an item, move the start up
    if (gateInstructionsStart >= NUM_INSTRUCTIONS)
    {
        gateInstructionsStart = 0;
    }

    return oldestInstruction;
}

//==============================================================================
// Hardware actions
//==============================================================================
void openGate()
{
    #ifdef DEBUG
    Serial.println("CMD Opening gate");
    #endif
    digitalWrite(PIN_OPEN_GATE, PIN_ON);
    delay(PIN_WRITE_DURATION);
    digitalWrite(PIN_OPEN_GATE, PIN_OFF);
    #ifdef DEBUG
    Serial.println("CMD Done Opening gate");
    #endif
}

void stopGate()
{
    #ifdef DEBUG
    Serial.println("CMD Stopping gate");
    #endif
    digitalWrite(PIN_STOP_GATE, PIN_ON);
    delay(PIN_WRITE_DURATION);
    digitalWrite(PIN_STOP_GATE, PIN_OFF);
    #ifdef DEBUG
    Serial.println("CMD Done Stopping gate");
    #endif
}

void closeGate()
{
    #ifdef DEBUG
    Serial.println("CMD Closing gate");
    #endif
    digitalWrite(PIN_CLOSE_GATE, PIN_ON);
    delay(PIN_WRITE_DURATION);
    digitalWrite(PIN_CLOSE_GATE, PIN_OFF);
    #ifdef DEBUG
    Serial.println("CMD Done Closing gate");
    #endif
}

//==============================================================================
// Xbee methods
//==============================================================================
void receiveCommand_series1()
{
    // Read packet and store in buffer
    if (Serial.available() > 0)
    {
        strBuff = Serial.readStringUntil('\n');

        #ifdef DEBUG
        Serial.println(strBuff);
        #endif
        Serial.flush();
        processCommandBuffer();
    }
    else
    {
        // Read packet and store in buffer
            xbee.readPacket();

            if (xbee.getResponse().isAvailable())
            {
                if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) // RX_16_RESPONSE
                {
                    xbee.getResponse().getZBRxResponse(rx);
                    data = (char *)rx.getData();
                    strBuff = String(data);

                    // Series 1 modules don't seem to ACK here, getOption() is 193 or 194 depending on Broadcast or direct msg.
                    // https://github.com/andrewrapp/xbee-arduino/blob/master/examples/Series1_Rx/Series1_Rx.pde#L77
                    // if (rx.getOption() == ZB_PACKET_ACKNOWLEDGED)
                    // {
                    //     Serial.println("the sender got an ACK");
                    // }
                    // else
                    // {
                    //     Serial.println("we got it (obviously) but sender didn't get an ACK");
                    // }

                    #ifdef DEBUG
                    Serial.println(data);
                    #endif
                    processCommandBuffer();
                }
            }
            else if (xbee.getResponse().isError())
            {
                Serial.print("Error reading packet.  Error code: ");
                Serial.println(xbee.getResponse().getErrorCode());

                xbee.getResponse().getZBRxResponse(rx);
                data = (char *)rx.getData();
                Serial.println(data);
            }
            else {
                return; // no data
            }
    }
}

void receiveCommand_series3()
{
    // Read packet and store in buffer
    if (Serial.available() > 0)
    {
        strBuff = Serial.readStringUntil('\n');

        #ifdef DEBUG
        Serial.println(strBuff);
        #endif
        Serial.flush();
        processCommandBuffer();
    }
    else
    {
        // Read packet and store in buffer
        do
        {
            //attempt to read a packet
            xbee.readPacket();

            if (xbee.getResponse().isAvailable())
            {
                if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) // RX_16_RESPONSE
                {
                    xbee.getResponse().getZBRxResponse(rx);
                    data = (char *)rx.getData();
                    strBuff = String(data);

                    #ifdef DEBUG
                    if (rx.getOption() == ZB_PACKET_ACKNOWLEDGED)
                    {
                        Serial.println("the sender got an ACK");
                    }
                    else
                    {
                        Serial.println("we got it (obviously) but sender didn't get an ACK");
                    }
                    #endif

                    #ifdef DEBUG
                    Serial.println(data);
                    #endif
                    processCommandBuffer();
                }
            }
            #ifdef DEBUG
            else if (xbee.getResponse().isError())
            {
                Serial.print("Error reading packet.  Error code: ");
                Serial.println(xbee.getResponse().getErrorCode());
            }
            #endif
            else {
                return; // no data
            }
        } while (xbee.getResponse().isAvailable());
    }
}

void processCommandBuffer() {
    #ifdef DEBUG
    Serial.println("Running buffer processing");
    #endif
    // Parse buffer
    int commaIndex = strBuff.indexOf(',');
    String strCommand = strBuff.substring(0, commaIndex);
    String strStop = strBuff.substring(commaIndex + 1);

    int command = strCommand.toInt();
    bool stop = (bool)strStop.toInt();

    // Clear buffer
    data = 0;

    // Process command
    if (command > 0)
    {
        #ifdef DEBUG
        Serial.print("RECV ");
        Serial.print(command);
        Serial.print(" ACK STOP=");
        Serial.println(stop);
        #endif
        addInstruction(command);
        lastReceive = millis();

        if (stop == false)
        {
            dataWaitThreshold = NEXT_PACKET_WAIT_THRESHOLD;
            #ifdef DEBUG
            Serial.print("EXPECT STOP WITHIN ");
            Serial.print(NEXT_PACKET_WAIT_THRESHOLD);
            Serial.println(" MS");
            #endif
        }
    }
    strBuff = "";
}

void sendState(char state)
{
    // Transmit to serial
    // XBeeAddress64 addr64 = XBeeAddress64(0x0013A200, 0x41C429AF);
    // ZBTxRequest zbTx = ZBTxRequest(addr64, state, sizeof(state));

    // Broadcast
    // ZBTxRequest zbTx = ZBTxRequest();
    // zbTx.setPayload(state);
    // zbTx.setPayloadLength(sizeof(state));

    // Hacky thing because sending a single char always sends 00
    uint8_t payload[1];
    payload[0] = state;

    XBeeAddress64 addr64 = XBeeAddress64(0x00000000, 0x0000FFFF); // Means broadcast
    ZBTxRequest zbTx = ZBTxRequest(addr64, payload, sizeof(payload));

    xbee.send(zbTx);
    #ifdef DEBUG
    Serial.print("FINISH SEND ");
    Serial.println(state);
    #endif
}
