package com.carvellwakeman.gatecontroller.ui.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.carvellwakeman.gatecontroller.R
import com.carvellwakeman.gatecontroller.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val myWebView: WebView = binding.webview
        myWebView.settings.javaScriptEnabled = true;
        myWebView.settings.databaseEnabled = true;
        myWebView.settings.loadWithOverviewMode = true;
        myWebView.settings.useWideViewPort = true;
        myWebView.settings.builtInZoomControls = true;
        myWebView.settings.displayZoomControls = true;
        myWebView.settings.domStorageEnabled = true;


        //myWebView.webViewClient = object : WebViewClient() {
        //    override fun shouldOverrideUrlLoading(
        //        view: WebView?,
        //        request: WebResourceRequest?
        //    ): Boolean {
        //        if (Uri.parse(request?.url.toString()).host == getString(R.string.website_domain)) {
        //            return false
        //        }
        //        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(request?.url.toString()))
        //        startActivity(intent)
        //        return true
        //    }
        //}

        val sharedPrefs = activity?.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        if (sharedPrefs !== null) {
            val myUrl = sharedPrefs.getString("url_pref_key", "")
            myWebView.loadUrl(myUrl.toString())
        }


        //val textView: TextView = binding.textHome
        //homeViewModel.text.observe(viewLifecycleOwner, Observer {
        //    textView.text = it
        //})
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}