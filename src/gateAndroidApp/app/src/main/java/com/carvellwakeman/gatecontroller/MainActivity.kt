package com.carvellwakeman.gatecontroller

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.Menu
import android.webkit.WebView
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import androidx.core.view.marginLeft
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import com.carvellwakeman.gatecontroller.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.appBarMain.fabRefresh.setOnClickListener { view ->
            val sharedPrefs = this@MainActivity
                .getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
            val myUrl = sharedPrefs.getString("url_pref_key", "")

            val alertDialog = AlertDialog.Builder(view.context)

            val editText: EditText = EditText(view.context);
            editText.setHint(R.string.url_hint)
            editText.setText(myUrl);

            val layoutName = LinearLayout(view.context)
            layoutName.orientation = LinearLayout.VERTICAL
            layoutName.setPadding(32, 0, 32, 0)
            layoutName.addView(editText) // displays the user input bar

            alertDialog.apply {
                //setIcon(R.drawable.ic_hello)
                setView(layoutName)

                setTitle("Set Gate Url")
                setMessage("Enter the base url or IP address only")
                setPositiveButton("Save") { _: DialogInterface?, _: Int ->
                    Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show()

                    if (!editText.text.isNullOrEmpty()){
                        sharedPrefs.edit { putString("url_pref_key", editText.text.toString()); apply() }
                        val myWebView: WebView = findViewById(R.id.webview)
                        myWebView.loadUrl(editText.text.toString())
                    }
                }

                setNegativeButton("Refresh") { _, _ ->
                    val myWebView: WebView = findViewById(R.id.webview)
                    myWebView.reload()
                    myWebView.clearCache(true)
                }

                setNeutralButton("Cancel") { _, _ ->
                    Toast.makeText(context, "Cancel", Toast.LENGTH_SHORT).show()
                }
                //setOnDismissListener {
                //    Toast.makeText(context, "Cancel2", Toast.LENGTH_SHORT).show()
                //}

            }.create().show()
        }

//        val drawerLayout: DrawerLayout = binding.drawerLayout
//        val navView: NavigationView = binding.navView
//        val navController = findNavController(R.id.nav_host_fragment_content_main)
//        // Passing each menu ID as a set of Ids because each
//        // menu should be considered as top level destinations.
//        appBarConfiguration = AppBarConfiguration(
//            setOf(
//                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow
//            ), drawerLayout
//        )
//
//        navView.setupWithNavController(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}